<?php

require_once "App/Main/Models/User.php";

$permissions = ["umu"=>1, "admin"=>1, "teacher"=>"1,2", "expert"=>1, "zavkaf"=>1];
$roles = ["admin"=>1,"zavkaf"=>2,"umu"=>3,"expert"=>4,"teacher"=>5];
$labels = ["admin"=>"Администраторы","zavkaf"=>"Заведующие кафедрой","umu"=>"Сотрудники УМУ","expert"=>"Эксперты","teacher"=>"Преподаватели"];

class Common {
	public static function Test($var) {
		return $var/2;
	}
}

class Request {
	public static function get() {
		$Request = new Request();
		if (isset($_GET)) {
			foreach ($_GET as $key => $value) {
				$Request->$key = str_replace("\?", "", $value);
				$Request->$key = str_replace("?", "", $value);
			}
			return $Request;
		}
	}

	public static function post() {
		$Request = new Request();
		if (isset($_POST)) {
			foreach ($_POST as $key => $value) {
				$Request->$key = $value;
			}
			return $Request;
		}
	}
}

class Validator {
	public static function String($str) {
		return htmlspecialchars(stripslashes($str));
	}

	public static function Reg($data) {
		unset($data->password_check);
		$data->password = md5($data->password);
		print_r($data);
		return $data;
	}

	public static function WisSave($data) {
		if (!isset($data['title']) || !isset($data['description']) || !isset($data['phone']) || !isset($data['email'])) {
			return false;
		}
		return true;
	}

	public static function SetSave($data) {
		global $Me;
		if ((isset($data['password']) && !empty($data['password'])) && (isset($data['password-check']) && !empty($data['password-check']))) {
			if ($data['password'] == $data['password-check']) {
				unset($data['password-check']);
				$data['password'] = md5($data['password']);
			}
			else {
				echo "Пароли не совпадают";
				return false;
			}
		}
		if ($ch = Database::Query("SELECT * FROM users WHERE login = $data[login]")) {
			$ch = mysqli_fetch_all($ch);
			if (count($ch) > 1) { echo "Логин занят"; return false; }
			elseif ($ch[0][1] != $Me->Profile->login) { echo "Логин занят"; return false; }
		}

		unset($data['password']);
		unset($data['password-check']);

		return $data;
	}
}

class Dates {
	/*
	отображение даты и времени
	день рождения (9.03.1700)
	самоуничтожение (осталось N дней/недель) MAX=14 days
	время и дата сообщений
	 */
	public function GetAge($bday) { //формат bday?
		return 5;
	}

	public function GetDate($date) {
		//return date();
	}


}

class Me {
	
	public $Profile;

	public function __construct() {
		//$Me = new Me();
		$this->Profile = new User($_SESSION["id"]);
		//return $Me;
	}
}

function dd($var) {
	highlight_string("<?php\n\$var =\n" . var_export($var, true) . ";\n?>"); die();
}