<?php

class ApiController extends Controllers {
	public function GetPulpits() {
		$id = Request::Get()->id;
		$data = mysqli_fetch_all(Database::Query("SELECT * FROM pulpits WHERE faculty_id = '$id'"));
		$json = [];
		foreach ($data as $d) {
			$json[] = ["id"=>$d[0], "title"=>$d[1]];
		}

		echo json_encode($json);
	}

	public function GetFaculties() {
		$data = mysqli_fetch_all(Database::Query("SELECT * FROM faculties"));
		$json = [];
		foreach ($data as $d) {
			$json[] = ["id"=>$d[0], "title"=>$d[1]];
		}

		echo json_encode($json);
	}

	public function GetDirections() {
		$id = Request::Get()->id;
		$data = mysqli_fetch_all(Database::Query("SELECT * FROM directions WHERE faculty_id = '$id'"));
		$json = [];
		foreach ($data as $d) {
			$json[] = ["id"=>$d[0], "title"=>$d[1]];
		}

		echo json_encode($json);
	}

	public function GetProfiles() {
		$id = Request::Get()->id;
		$id2 = Request::Get()->id2;
		$data = mysqli_fetch_all(Database::Query("SELECT * FROM profiles WHERE direction_id = '$id2' AND study_level = '$id'"));
		$json = [];
		foreach ($data as $d) {
			$json[] = ["id"=>$d[0], "title"=>$d[1]];
		}

		echo json_encode($json);
	}

	public function GetStudyTypes() {
		$data = mysqli_fetch_all(Database::Query("SELECT * FROM study_types"));
		$json = [];
		foreach ($data as $d) {
			$json[] = ["id"=>$d[0], "title"=>$d[1]];
		}

		echo json_encode($json);
	}

	public function GetOpopManagers() {
		$id = Request::Get()->id;
		$data = mysqli_fetch_all(Database::Query("SELECT * FROM opop_managers WHERE direction_id = '$id'"));
		$json = [];
		foreach ($data as $d) {
			$user = mysqli_fetch_object(Database::Query("SELECT * FROM users WHERE id = '$d[1]'"));
			$json[] = ["id"=>$d[0], "title"=>"$user->name $user->surname $user->lastname"];
		}

		echo json_encode($json);
	}
}