<?php
require_once "App\Main\Models\Documents.php";
require_once "App\Main\Models\Templates.php";

require_once 'dompdf/lib/html5lib/Parser.php';
require_once 'dompdf/lib/php-font-lib/src/FontLib/Autoloader.php';
require_once 'dompdf/lib/php-svg-lib/src/autoload.php';
require_once 'dompdf/src/Autoloader.php';
Dompdf\Autoloader::register();
use Dompdf\Dompdf;

// require "vendor/autoload.php";
// use Spipu\Html2Pdf\Html2Pdf;

class DocumentsController extends Controllers {

	public static function Index() {
		$data['documents'] = Documents::GetAll();
		return self::ShowView("Documents", "Index", $data);
	}

	public static function New() {
		if (isset(Request::get()->id)) {
			if ($tm = Templates::Get(Request::get()->id)) {
				// $tm->document = Templates::ParseOut($tm->document);
				$data['template'] = $tm;
				$data['study_types'] = mysqli_fetch_all(Database::Query("SELECT * FROM study_types"));
				// var_dump($data); die();
				return self::ShowView("Documents", "New", $data);
			}
			echo "Шаблон не найден";
		}
		else {
			$data['templates'] = Templates::GetAllConfirmed();
			return self::ShowView("Documents", "Templates", $data);
		}
	}

	public static function Store() {
		Documents::Store($_POST);
		header("Location: /documents");
	}

	public static function StorePart() {
		//dd($_POST);
		Documents::StorePart(Request::get()->id, Request::get()->part, $_POST);
		header("Location: /documents/edit/".Request::get()->id."?last=".Request::get()->part);
	}

	public static function Show() {
		$data['document'] = new Documents(Request::get()->id);
		self::ShowView("Documents", "Show", $data);
	}

	public static function Edit() {
		if (isset(Request::get()->part)) {
			$data = [];
			$data['doc'] = Documents::Get(Request::get()->id);
			// var_dump(Documents::GetPart(Request::get()->id, Request::get()->part)); die();
			if ($part = Documents::GetPart(Request::get()->id, Request::get()->part)) {
				// dd($part);
				$partT = Templates::GetPart(Request::get()->part);
				$partT->document = Documents::ParseMain($part->document, new Documents(Request::get()->id));
				$data['part'] = $partT;
				self::ShowView("Documents", "NewPart", $data);
			}
			else {
				$part = Templates::GetPart(Request::get()->part);
				$part->document = Templates::ParseOut($part->document);
				$part->document = Documents::ParseMain($part->document, new Documents(Request::get()->id));
				$data['part'] = $part;
				self::ShowView("Documents", "NewPart", $data);
			}
		}
		else {
			$document = Documents::Get(Request::get()->id);
			$parts = Templates::GetParts($document->template_id);
			return self::ShowView("Documents", "Selectpart", ['parts'=>$parts, 'doc'=>$document]);
		}
	}

	public static function Update() {
		Documents::Update($_POST, Request::get()->id);
		header("Location: /documents");
	}

	public static function Delete() {
		Documents::Delete(Request::get()->id);
		header("Location: /documents");
	}

	public static function Check() {
		Documents::Status(Request::get()->id);
		if (Request::get()->redirect == "show") {
			header("Location: /documents/show/".Request::get()->id);
		}
		else {
			header("Location: /documents");
		}
	}

	public static function Expert() {
		$data['documents'] = Documents::GetForCheck();
		return self::ShowView("Documents", "Expert", $data);
	}

	public static function Decision() {
		return self::ShowView("Documents", "ExpertD");
	}

	public static function ExSave() {
		Documents::Update($_POST, Request::Get()->id);
		header("Location: /documents/Expert");

	}

	public static function Subjects() {
		$data['document'] = Documents::Get(Request::get()->id);
		self::ShowView("Documents", "Subjects", $data);
	}

	public static function Matrix() {
		$data['document'] = Documents::Get(Request::get()->id);
		self::ShowView("Documents", "Matrix", $data);
	}

	public static function Export() {
		// dd(get_declared_classes());
		include('tcpdf/tcpdf.php');
		$doc = new Documents(Request::get()->id);

		echo "<style>body { margin: 0 auto; }</style> <div style='text-align: -webkit-center; background: #555;'><div style='width: 650px; background: #fff; padding: 45px; text-align: left;'>".$doc->document."</div></div>";
		die();
		
		// $doc->document = iconv('UTF-8', 'UTF-8', $doc->document);
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$pdf->SetPrintHeader(false);
		// set document information
		// $pdf->SetCreator(PDF_CREATOR);
		// $pdf->SetAuthor('Nicola Asuni');
		// $pdf->SetTitle('TCPDF Example 006');
		// $pdf->SetSubject('TCPDF Tutorial');
		// $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

		// set default header data
		// $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING);

		// // set header and footer fonts
		// $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		// $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		// $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, 0, PDF_MARGIN_RIGHT);
		// $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		// $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		// $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		// $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		$pdf->SetLineStyle(array('width' => 0, 'cap' => 'butt', 'join' => 'miter', 'dash' => 4, 'color' => array(255, 255, 255)));
		// set some language-dependent strings (optional)
		// if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		// 	require_once(dirname(__FILE__).'/lang/eng.php');
		// 	$pdf->setLanguageArray($l);
		// }

		// ---------------------------------------------------------

		// set font
		$pdf->SetFont('dejavusans', '', 10);

		// add a page
		$pdf->AddPage();

		$html = "<!DOCTYPE html><html><head><meta charset='utf-8'><style>body { font-family: 
			Arial; } table {border: none;}</style><title></title></head><body>".$doc->document."</body></html>";
		$pdf->writeHTML($html, true, false, true, false, '');
		$pdf->lastPage();
		$pdf->Output($doc->title.'.pdf', 'I');
		// $html2pdf = new Html2Pdf();
		// $html2pdf->writeHTML($html);
		// $html2pdf->output()->SetFont(‘times’, ‘BI’, 20, “, ‘false’);

		// $dompdf->load_html($html);
		// $dompdf->render();

		// $dompdf->stream("$doc->title.pdf");
		header("Location: /documents/show/".Request::get()->id);

	}

}