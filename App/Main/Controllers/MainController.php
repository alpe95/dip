<?php
require_once 'App\Main\System\Controllers.php';
require_once 'App\Main\Models\Main.php';
require_once 'App\Main\Models\Templates.php';

class MainController extends Controllers {

	public function Index() {
		global $Me;
		if (in_array(2, $Me->Profile->roles)){
			require_once "App\Main\Controllers\TemplatesController.php";
			TemplatesController::Index();
		}
		elseif (in_array(4, $Me->Profile->roles)) {
			require_once "App\Main\Controllers\DocumentsController.php";
			DocumentsController::Index();
		}
		elseif (in_array(1, $Me->Profile->roles)) {
			require_once "App\Main\Controllers\UsersController.php";
			$_GET['type'] = "admin";
			UsersController::Get();
		}
		elseif (in_array(3, $Me->Profile->roles)) {
			require_once "App\Main\Controllers\DocumentsController.php";
			DocumentsController::Expert();
		}
	}

	public static function Get($id) {
		if ($Data['wis'] = Wisitka::Get($id)) return self::ShowViewNoTemplate("Main","Show", $Data);
		else return self::ShowViewNoTemplate("Main","Error", $Data);
	}

	public function Create() {
		return self::ShowViewNoTemplate("Main","Create");
	}

	public function Edit() {
		$wis = Wisitka::Get($_GET['id']);
		return self::ShowView("Main","Create");
	}

	public function Test() {
		echo 'some fucking shit';
		echo "<p>".$_GET['id']."</p>";
	}

	public function Exit() {
		session_destroy();
		header("location: /");
	}
}