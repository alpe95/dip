<?php

require_once 'App/Main/Models/User.php';
require_once 'App/Main/Models/parts.php';
Controllers::CheckAccess(2);

class PartsController extends Controllers {
	
	public function Index() {
		$data['parts'] = parts::GetAllAsObjects();
		return self::ShowView("parts", "Index", $data);
	}

	public function Edit() {
		$data['part'] = parts::Get(Request::get()->id);
		$data['template'] = mysqli_fetch_all(Database::Query("SELECT * FROM docs_templates WHERE id='".$data['part']->template_id."'"))[0];
		return self::ShowView("Parts", "Edit", $data);
	}

	public function Show() {
		$data['template'] = parts::Get(Request::get()->id);
		$data['template']->document = parts::ParseOut($data['template']->document);
		return self::ShowView("parts", "Show", $data);
	}

	public function Update() {
		$par = Parts::Get(Request::get()->id)->template_id;
		parts::Update($_POST, Request::get()->id);
		header("Location: /templates/edit/".$par."?last=".Request::get()->id);
	}

	public function New() {
		//$data['users'] = User::GetUMU();
		return self::ShowView("parts", "Create");
	}

	public function Store() {
		parts::Store($_POST);
		header("Location: /templates/edit/".Request::get()->id);
	}

	public static function Delete() {
		$par = Parts::Get(Request::get()->id)->template_id;
		parts::Delete(Request::get()->id);
		header("Location: /templates/edit/".$par);
	}

	public static function Status() {
		$par = Parts::Get(Request::get()->id)->template_id;
		parts::Status(Request::get()->id);
		header("Location: /templates/edit/".$par);
	}

}

