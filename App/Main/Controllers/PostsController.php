<?php

require_once "App\Main\Models\Posts.php";

class PostsController extends Controllers {

	public static function Index() {
		global $Me;
		if(in_array(1, $Me->Profile->roles)) {
			$posts = posts::GetAll();
			return self::ShowView("Posts", "Index", ["posts"=>$posts]);
		}
		echo "Доступ запрещен.";
	}

	public static function New() {
		global $Me;
		if(in_array(1, $Me->Profile->roles)) {
			$posts = posts::GetAll();
			return self::ShowView("Posts", "New");
		}
		echo "Доступ запрещен.";
	}

	public static function Edit() {
		global $Me;
		if(in_array(1, $Me->Profile->roles)) {
			$pulpit = posts::Get($_GET['id']);
			return self::ShowView("Posts", "Edit", ["pulpit"=>$pulpit]);
		}
		echo "Доступ запрещен.";
	}

	public static function Store() {
		global $Me;
		if(in_array(1, $Me->Profile->roles)) {
			posts::Create($_POST);
			header("Location: /posts");
			return;
		}
		echo "Доступ запрещен.";
	}

	public static function Update() {
		global $Me;
		if(in_array(1, $Me->Profile->roles)) {
			posts::Update($_POST, $_GET['id']);
			header("Location: /posts");
			return;
		}
		echo "Доступ запрещен.";
	}

}