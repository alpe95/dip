<?php

require_once "App\Main\Models\Pulpits.php";

class PulpitsController extends Controllers {

	public static function Index() {
		global $Me;
		if(in_array(1, $Me->Profile->roles)) {
			$pulpits = Pulpits::GetAll();
			return self::ShowView("Pulpits", "Index", ["pulpits"=>$pulpits]);
		}
		echo "Доступ запрещен.";
	}

	public static function New() {
		global $Me;
		if(in_array(1, $Me->Profile->roles)) {
			$pulpits = Pulpits::GetAll();
			return self::ShowView("Pulpits", "New");
		}
		echo "Доступ запрещен.";
	}

	public static function Edit() {
		global $Me;
		if(in_array(1, $Me->Profile->roles)) {
			$pulpit = Pulpits::Get($_GET['id']);
			return self::ShowView("Pulpits", "Edit", ["pulpit"=>$pulpit]);
		}
		echo "Доступ запрещен.";
	}

	public static function Store() {
		global $Me;
		if(in_array(1, $Me->Profile->roles)) {
			Pulpits::Create($_POST);
			header("Location: /pulpits");
			return;
		}
		echo "Доступ запрещен.";
	}

	public static function Update() {
		global $Me;
		if(in_array(1, $Me->Profile->roles)) {
			Pulpits::Update($_POST, $_GET['id']);
			header("Location: /pulpits");
			return;
		}
		echo "Доступ запрещен.";
	}

}