<?php

require_once 'App/Main/Models/User.php';

class SettingsController extends Controllers {

	public function Index() {
		return self::ShowView("Settings","Index");
	}

	public static function Save() {
		//print_r($_GET);
		// exit();
		User::Update($_POST); 
	}

	public static function Newava() {
		User::SaveAvatar($_FILES['avatar']);
	}
}

