<?php

require_once 'App/Main/Models/User.php';
require_once 'App/Main/Models/Templates.php';
Controllers::CheckAccess(2);

class TemplatesController extends Controllers {
	
	public function Index() {
		$data['templates'] = Templates::GetAllAsObjects();
		return self::ShowView("Templates", "Index", $data);
	}

	public function Edit() {
		$data['template'] = Templates::Get(Request::get()->id);
		$data['parts'] = Templates::GetParts(Request::get()->id);
		return self::ShowView("Templates", "Edit", $data);
	}

	public static function EditTemplate() {
		$data = [];
		$data["template"] = Templates::Get(Request::get()->id);
		self::ShowView("Templates", "EditTemplate", $data);
	}

	public static function Updateinfo() {
		Templates::Updateinfo(Request::get()->id, $_POST);
		header("Location: /templates/edit/".Request::get()->id);
	}

	public function EditPart() {
		$data['template'] = Templates::GetPart(Request::get()->id);
		return self::ShowView("Templates", "EditPart", $data);
	}

	public function Show() {
		$res = "";
		$parts = Templates::GetParts(Request::get()->id);
		$template = Templates::Get(Request::get()->id);
		foreach ($parts as $part) {
			$res.=Templates::ParseOut($part[3]);
		}

		return self::ShowView("Templates", "Show", ["document"=>$res, "template"=>$template]);
	}

	public function Update() {
		Templates::Update($_POST, Request::get()->id);
		header("Location: /templates");
	}

	public function New() {
		//$data['users'] = User::GetUMU();
		return self::ShowView("Templates", "Create");
	}

	public function Store() {
		global $Me;
		$_POST['user_id'] = $Me->Profile->id;
		Templates::Store($_POST);
		header("Location: /templates");
	}

	public static function Delete() {
		Templates::Delete(Request::get()->id);
		header("Location: /templates");
	}

	public static function Status() {
		Templates::Status(Request::get()->id);
		header("Location: /templates");
	}

}

