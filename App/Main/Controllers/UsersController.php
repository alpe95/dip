<?php

require_once 'App/Main/Models/User.php';
require_once 'App/Main/Models/Faculties.php';
require_once 'App/Main/Models/Data.php';
Controllers::CheckAccess(1);

class UsersController extends Controllers {

	public function Get() {
		$data['users'] = User::GetTable();
		$data['label'] = "Пользователи";	
		return self::ShowView("User", "Show", $data);
	}

	public function Create() {
		$data['faculties'] = Faculties::GetAll();
		$data['posts'] = Data::GetPosts();
		$data['sc_levels'] = Data::GetScLevels();
		$data['sc_titles'] = Data::GetScTitles();
		$data['roles'] = Data::GetRoles();
		$data['label'] = "Пользователи";
		$data['login'] = "user_".((mysqli_fetch_all(Database::Query("SELECT max(id) from users"))[0][0])+1);
		return self::ShowView("User", "Create", $data);
	}
	
	public static function Store() {

		User::Create($_POST);
		header("Location: /users/get");
	}

	public function Edit() {
		$data['faculties'] = Faculties::GetAll();
		$data['label'] = "Пользователь";
		$data['user'] = new User(Request::get()->id);
		$data['pulpits'] = mysqli_fetch_all(Database::Query("SELECT * FROM pulpits WHERE faculty_id='".$data['user']->faculty_id."'"));
		$data['roleslist'] = mysqli_fetch_all(Database::Query("SELECT * FROM roles"));
		$data['sc_levels'] = mysqli_fetch_all(Database::Query("SELECT * FROM sc_levels"));
		$data['sc_titles'] = mysqli_fetch_all(Database::Query("SELECT * FROM sc_titles"));
		// dd($data['user']);
		return self::ShowView("User", "Edit", $data);
	}

	public static function Update() {
		User::Update($_POST, Request::get()->id);
		header("Location: /users/get");
	}

	public static function Delete() {
		User::Delete(Request::get()->id);
		echo $_SERVER['REQUEST_URI'];
		header("Location: /users/get");
	}

}