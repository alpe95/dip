<?php

class Data extends Models {

	public function GetPosts() {
		if ($res = Database::Query("SELECT * FROM posts")) {
			return mysqli_fetch_all($res);
		}
		return false;
	}

	public function GetScLevels() {
		if ($res = Database::Query("SELECT * FROM sc_levels")) {
			return mysqli_fetch_all($res);
		}
		return false;
	}

	public function GetScTitles() {
		if ($res = Database::Query("SELECT * FROM sc_titles")) {
			return mysqli_fetch_all($res);
		}
		return false;
	}

	public function GetRoles() {
		if ($res = Database::Query("SELECT * FROM roles")) {
			return mysqli_fetch_all($res);
		}
		return false;
	}

}