<?php
require_once("App\Main\Models\User.php");

class Documents extends Models {

	public $id;
	public $title;
	public $accepted_year;
	public $user_id;
	public $template_id;
	public $status;
	public $created;
	public $updated;

	public function __construct($id) {
		
		if ($temp = self::Get($id)) {
			// unset($user->password);
			[$this->id,
			$this->title,
			$this->user_id,
			$this->template_id,
			$this->accepted_year,
			$this->opop_manager,
			$this->status,
			$this->faculty_id,
			$this->pulpit_id,
			$this->direction_id,
			$this->profile_id,
			$this->comment,
			$this->created,
			$this->updated,
			$this->study_level,
			$this->study_type] = array_values((array)$temp);

			$managers = mysqli_fetch_all(Database::Query("SELECT * FROM opop_managers WHERE id='$this->opop_manager'"))[0];
			$opop_manager_info = User::Get($managers[1]);
			$faculty_info = mysqli_fetch_all(Database::Query("SELECT * FROM faculties WHERE id = '$this->faculty_id'"))[0];
			$pulpit_info = mysqli_fetch_all(Database::Query("SELECT * FROM pulpits WHERE id = '$this->pulpit_id'"))[0];
			$profile_info = mysqli_fetch_all(Database::Query("SELECT * FROM profiles WHERE id = '$this->profile_id'"))[0];
			$stypeinfo_info = mysqli_fetch_all(Database::Query("SELECT * FROM study_types WHERE id = '$this->study_type'"))[0];
			$slevelinfo_info = mysqli_fetch_all(Database::Query("SELECT * FROM study_levels WHERE id = '$this->study_level'"))[0];



			$this->string['opop_manager'] = $opop_manager_info->surname." ".$opop_manager_info->name." ".$opop_manager_info->lastname;

			$this->string['direction'] = mysqli_fetch_all(Database::Query("SELECT * FROM directions WHERE id='".$this->direction_id."'"))[0][1];

			$dekan = User::Get($faculty_info[4]);
			$this->string['facultymanager'] = $dekan->surname." ".$dekan->name." ".$dekan->lastname;
			$this->string['faculty'] = $faculty_info[1];
			$this->string['pulpit'] = $pulpit_info[1];

			$this->string['profile'] = $profile_info[1];
			$this->string['study_type'] = $stypeinfo_info[1];
			$this->string['study_level'] = $slevelinfo_info[1];
			// // dd($faculty_info);
			$doc_text = "";
			$doc_parts = mysqli_fetch_all(Database::Query("SELECT * FROM parts WHERE template_id='$this->template_id'"));
			foreach ($doc_parts as $part) {
				$datt = mysqli_fetch_all(Database::Query("SELECT * FROM doc_parts WHERE part_id='$part[0]'"));
				if (count($datt) > 0) {
					$datt = $datt[0][3];
					$doc_text.=self::ParseOut($part[3],$datt);
				}
				else {
					$doc_text.=$part[3];
				}
			}
			$this->document = self::ParseMain($doc_text, $this);
			// $this->document = $doc_text;
		}

		return false;
	}

	public static function Get($id) {

		if ($query = Database::Query("SELECT * FROM documents WHERE id = $id")) {

			$query = mysqli_fetch_object($query);
			return $query;
		}

		return false; 
	}

	public static function GetPart($document, $part) {
		if ($query = Database::Query("SELECT * FROM doc_parts WHERE document_id='$document' AND part_id='$part'")) {
			if ($query->num_rows==0) return false;
			// var_dump($query); die();
			$query = mysqli_fetch_object($query);
			// dd($query->document);
			// dd($query);
			$query->document = self::Parse(Templates::GetPart($query->part_id)->document, $query->document);
			return $query;
		}
		return false;
	}

	public static function GetForms($id) {

		if ($query = Database::Query("SELECT * FROM documents WHERE id = $id")) {

			$query = mysqli_fetch_object($query);
			$query->document = self::Parse(Templates::Get($query->template_id)->document, $query->document);
		return $query;
		}

		return false; 
	}

	public static function Store($data) {
		$data['user_id'] = $_SESSION['id'];
		$data['template_id'] = $_GET['id'];
		$data['status'] = 1;
		if ($data['profile_id'] == 0) unset($data['profile_id']);
		// dd($data);
		// dd($data);
		if(Database::Insert("documents", $data)){
			return true;
		}
		return false;
		die();
	}

	public static function StorePart($doc, $part, $data) {
		foreach ($data as $key => $value) {
			$data[$key] = addcslashes($value, "\"");
		}
		$document = json_encode($data,  JSON_UNESCAPED_UNICODE);
		// dd($document);
		$data = ["document_id"=>$doc, "part_id"=>$part, "document"=>$document];

		if (Database::Query("SELECT * FROM doc_parts WHERE document_id='$doc' AND part_id='$part'")->num_rows!=0) {
			if(Database::Update("doc_parts", ['document'=>$document], ['document_id'=>$doc, 'part_id'=>$part])){
				return "true1";
			}
		}
		else {
			if(Database::Insert("doc_parts", $data)){
				return "true2";
			}
		}
		return false;
	}

	public static function GetForCheck() {
		if ($temps = Database::Query("Select * FROM documents WHERE status='2' AND expert_id IN ('$_SESSION[id]','0')")) {
			return mysqli_fetch_all($temps);
		}
		return false;
	}

	public static function GetAll() {
		if ($temps = Database::Query("Select * FROM documents WHERE user_id='$_SESSION[id]'")) {
			return mysqli_fetch_all($temps);
		}
		return false;
	}

	public static function Update($data, $id) {
		// if (isset($data['title'])) {
		// 	$title = $data['title'];
		// 	unset($data['title']);
		// 	// dd($document);
		// 	$data = ["title"=>$title, "user_id"=>$_SESSION['id']];
		// }
		if (Database::Update("documents", $data, ['id'=>$id])){
			echo "ok";
			// exit();
		}
		else {
			echo "No!<br>";
			print_r($data);
			// exit();
		}
		
	}

	function json_decode_nice($json, $assoc = TRUE){
	    $json = str_replace("\\n","",$json);
	    $json = str_replace("\r","",$json);

	    $json = preg_replace('/([e{,]+)(\s*)([^"]+?)\s*:/','$1"$3":',$json);
	    $json = preg_replace('/(,)\s*}$/','}',$json);
	    $json = preg_replace("/\s+/", " ", $json);
	    return json_decode($json,$assoc);
	}

	public static function Parse($text, $data) {
		// dd(1);
		// $data = str_replace(search, replace, subject)
		$data = self::json_decode_nice($data, true);
		preg_match_all("/\[[a-z];[0-9]{1,};\]/", $text, $out);
		foreach ($out[0] as $o) {
			$o = preg_replace("/\[/", "", $o);
			$o = preg_replace("/\]/", "", $o);
			$o = preg_replace("/\(/", "\(", $o);
			$o = preg_replace("/\)/", "\)", $o);
			$n = explode(";", $o);
			$n[2] = stripslashes($n[2]);
			if ($n[0] == "l") {
				$text = preg_replace("/\[$o\]/", "<textarea class='$n[0] ckeditor' name='$n[0]$n[1]' placeholder='$n[2]'>".$data[$n[0].$n[1]]."</textarea>", $text);
			}
			else {
				$text = preg_replace("/\[$o\]/", "<input type='text' class='$n[0]' name='$n[0]$n[1]' placeholder='$n[2]' value='".$data[$n[0].$n[1]]."'>", $text);
			}
		}
		// $text = preg_replace("/\[opopmanager]/", "[Руководитель ОП]", $text);
		// $text = preg_replace("/\[direction]/", "[Направление подготовки]", $text);
		// $text = preg_replace("/\[direction]/", "[Направление подготовки]", $text);
		// $text = preg_replace("/\[profile]/", "[Профиль]", $text);
		// $text = preg_replace("/\[studytype]/", "[Форма обучения]", $text);
		// $text = preg_replace("/\[facultymanager]/", "[Декан]", $text);
		return $text;
	}

	public static function ParseMain($text, $obj) {
		// dd($obj);
		$text = preg_replace("/\[opopmanager]/", $obj->string['opop_manager'], $text);
		$text = preg_replace("/\[direction]/", $obj->string['direction'], $text);
		$text = preg_replace("/\[profile]/", $obj->string['profile'], $text);
		$text = preg_replace("/\[faculty]/", $obj->string['faculty'], $text);
		$text = preg_replace("/\[pulpit]/", $obj->string['pulpit'], $text);
		$text = preg_replace("/\[studytype]/", $obj->string['study_type'], $text);
		$text = preg_replace("/\[studylevel]/", $obj->string['study_level'], $text);
		$text = preg_replace("/\[facultymanager]/", $obj->string['facultymanager'], $text);
		return $text;
	}

	public static function ParseOut($text, $data) {
		$data = json_decode($data, true);
		preg_match_all("/\[[a-z];[0-9]{1,};\]/", $text, $out);
		foreach ($out[0] as $o) {
			$o = preg_replace("/\[/", "", $o);
			$o = preg_replace("/\]/", "", $o);
			$o = preg_replace("/\(/", "\(", $o);
			$o = preg_replace("/\)/", "\)", $o);
			$n = explode(";", $o);
			$n[2] = stripslashes($n[2]);
			$text = preg_replace("/\[$o\]/", $data[$n[0].$n[1]], $text);
		}
		// dd($text);
		// $text = preg_replace("/\[opopmanager\]/", $data[$n[0].$n[1]], $text);
		return $text;
	}

	public static function Delete($id) {
		Database::Query("DELETE FROM documents WHERE id='$id'");
	}

	public static function Status($id) {
		$doc = self::Get($id);
		if ($doc->status == 1) {
			Database::Update("documents", ['status'=>3], ['id'=>$id]);
		}
		elseif ($doc->status == 3) {
			Database::Update("documents", ['status'=>1], ['id'=>$id]);
		}
		else {
			Database::Update("documents", ['status'=>1], ['id'=>$id]);
		}
	}


}

?>