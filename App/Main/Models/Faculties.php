<?php

class Faculties extends Models {

	public static function Get($id) {
		if ($res = Database::Query("SELECT * FROM Faculties WHERE pulpits.id='$id'")) {
			return mysqli_fetch_object($res);
		}
		return false;
	}

	public static function GetAll() {
		if ($res = Database::Query("SELECT * FROM Faculties")) {
			return mysqli_fetch_all($res);
		}
		return false;
	}

}