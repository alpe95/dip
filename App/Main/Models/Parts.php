<?php

class Parts extends Models {

	public $id;
	public $template_id;
	public $title;
	public $document;

	public function __construct($id) {
		
		if ($temp = self::Get($id)) {
			[$this->id,
			$this->template_id,
			$this->title,
			$this->document] = array_values((array)$temp);
		}
		return false;
	}

	public static function Get($id) {
		if ($query = Database::Query("SELECT * FROM parts WHERE id = $id")) {
			$query = mysqli_fetch_object($query);
			return $query;
		}

		return false; 
	}

	public static function Delete($id) {
		$pr = mysqli_fetch_object(Database::Query("SELECT * from parts WHERE id='$id'"))->template_id;
		Database::Query("UPDATE docs_templates SET updated=CURRENT_TIMESTAMP WHERE id='$pr'");
		Database::Query("DELETE FROM parts WHERE id='$id'");

	}

	public static function Store($data) {
		//$data = Validator::Tem($data);

		$data['template_id'] = Request::get()->id;
		$data['document'] = self::Parse($data['document']);
		if (isset( Request::post()->for_teachers)) {
			$data['for_teachers'] = 1;
		}
		// dd($data);
		if(Database::Insert("parts", $data)){
			Database::Query("UPDATE docs_templates SET updated=CURRENT_TIMESTAMP WHERE id='$data[template_id]'");
			return true;
		}
		return false;
	}

	public static function GetAll() {
		if ($temps = Database::Query("Select * FROM parts")) {
			return mysqli_fetch_all($temps);
		}
		return false;
	}

	public static function GetAllAsObjects() {
		if ($temps = Database::Query("Select * FROM parts")) {
			$tids = mysqli_fetch_all($temps);
			$res = [];
			foreach ($tids as $t) {
				$res[] = new Templates($t[0]);
			}
			return $res;
		}
		return false;
	}

	public static function Update($data, $id) {
		$data['document'] = self::Parse($data['document']);
		if (Database::Update("parts", $data, ['id'=>$id])){
			echo "ok";
			$pr = mysqli_fetch_object(Database::Query("SELECT * from parts WHERE id='$id'"))->template_id;
			Database::Query("UPDATE docs_templates SET updated=CURRENT_TIMESTAMP WHERE id='$pr'");
		}
		else {
			echo "No!<br>";
			print_r($data);
			// exit();
		}
		
	}

	public static function Parse($text) {
		// $text = htmlspecialchars($text);
		$text = preg_replace("/\[n\]/", "[n;]", $text);
		$text = preg_replace("/\[t\]/", "[t;]", $text);
		$text = preg_replace("/\[l\]/", "[l;]", $text);
		preg_match_all("/\[[a-z];.*?\]/", $text, $out);
		preg_match_all("/\[[a-z];.*?;.*?\]/", $text, $out2);
		$out = array_diff($out[0], $out2[0]);
		$res = [];
		foreach ($out as $s) {
			$res[] = $s;
		}
		$out = $res;
		for ($i=0; $i<count($out); $i++) {
			$j = $i;
			while (preg_match("/\[.*?;$j;.*?\]/", $text)==1) $j++;
			$o = $out[$i];
			$o = preg_replace("/\[/", "", $o);
			$o = preg_replace("/\]/", "", $o);
			$o = preg_replace("/\(/", "\(", $o);
			$o = preg_replace("/\)/", "\)", $o);
			$n = explode(";", $o);
			if (preg_match("/\[.*?;.*?;.*?\]/", $out[$i]) == 0) $text = preg_replace("/\[$o\]/", "[".$n[0].";$j;".$n[1]."]", $text, 1);
		}
		// $text = preg_replace("/\\\(/", "(", $text);
		// $text = preg_replace("/\\\)/", ")", $text);
		return $text;
	}

	public static function ParseOut($text) {
		preg_match_all("/\[.*?;.*?\]/", $text, $out);
		foreach ($out[0] as $o) {
			$o = preg_replace("/\[/", "", $o);
			$o = preg_replace("/\]/", "", $o);
			$o = preg_replace("/\(/", "\(", $o);
			$o = preg_replace("/\)/", "\)", $o);
			$n = explode(";", $o);
			$n[2] = stripslashes($n[2]);
			if ($n[0] == "l") {
				$text = preg_replace("/\[$o\]/", "<textarea class='$n[0]' name='$n[0]$n[1]' placeholder='$n[2]'></textarea>", $text);
			}
			else {
				$text = preg_replace("/\[$o\]/", "<input type='text' class='$n[0]' name='$n[0]$n[1]' placeholder='$n[2]'>", $text);
			}
		}
		// $text = preg_replace("/\\\(/", "\(", $text);
		// $text = preg_replace("/\\\)/", "\)", $text);
		return $text;
	}

	public static function Status($id) {
		$tmp = self::Get($id);
		if ($tmp->status == 0) {
			Database::Update("parts", ['status'=>1], ['id'=>$id]);
		}
		elseif ($tmp->status == 1) {
			Database::Update("parts", ['status'=>0], ['id'=>$id]);
		}
		else {
			return;
		}
	}

}

?>