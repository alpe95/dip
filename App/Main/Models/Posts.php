<?php

class Posts extends Models {

	public static function Get($id) {
		if ($res = Database::Query("SELECT * FROM posts WHERE id='$id'")) {
			return mysqli_fetch_object($res);
		}
		return false;
	}

	public static function GetAll() {
		if ($res = Database::Query("SELECT * FROM posts")) {
			return mysqli_fetch_all($res);
		}
		return false;
	}

	public static function Create($data) {
		if (Database::Insert("posts", $data)) {
			return true;
		}
		return false;
	}

	public static function Update($data, $id) {
		if (Database::Update("posts", $data, ['id'=>$id])) {
			return true;
		}
		return false;
	}

	public static function Delete($id) {
		Database::Query("DELETE FROM posts WHERE id='$id'");
	}

}