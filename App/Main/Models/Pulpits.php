<?php

class Pulpits extends Models {

	public static function Get($id) {
		if ($res = Database::Query("SELECT pulpits.id, title, name, surname FROM pulpits, users WHERE pulpits.id='$id' AND manager=users.id")) {
			return mysqli_fetch_object($res);
		}
		return false;
	}

	public static function GetAll() {
		if ($res = Database::Query("SELECT pulpits.id, title, name, surname FROM pulpits, users WHERE manager=users.id")) {
			return mysqli_fetch_all($res);
		}
		return false;
	}

	public static function Create($data) {
		if (Database::Insert("pulpits", $data)) {
			return true;
		}
		return false;
	}

	public static function Update($data, $id) {
		if (Database::Update("pulpits", $data, ['id'=>$id])) {
			return true;
		}
		return false;
	}

	public static function Delete($id) {
		Database::Query("DELETE FROM pulpits WHERE id='$id'");
	}

}