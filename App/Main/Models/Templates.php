<?php
require_once "App\Main\Models\User.php";

class Templates extends Models {

	public $id;
	public $title;
	public $accepted_year;
	public $user;
	public $status;
	public $created;
	public $updated;
	public $comment;
	public $stat_text = ["Черновик", "Утвержден"];

	public function __construct($id) {
		
		if ($temp = self::Get($id)) {
			[$this->id,
			$this->title,
			$this->user,
			$this->study_level,
			$this->study_type,
			$this->status,
			$this->updated,
			$this->created,
			$this->comment] = array_values((array)$temp);

			$this->study_level = mysqli_fetch_all(Database::Query("SELECT * FROM study_levels WHERE id='$this->study_level'"))[0][1];
			$this->user_info = new User($this->user);
		}
		return false;
	}

	public static function Get($id) {

		if ($query = Database::Query("SELECT * FROM docs_templates WHERE id = $id")) {
			$query = mysqli_fetch_object($query);
			return $query;
		}

		return false; 
	}

	public static function GetPart($id) {
		if ($query = Database::Query("SELECT * FROM parts WHERE id='$id'")) {
			$query = mysqli_fetch_object($query);
			return $query;
		}
	}

	public static function GetParts($id) {
		if ($query = Database::Query("SELECT * FROM parts WHERE template_id='$id'")) {
			$query = mysqli_fetch_all($query);
			return $query;
		}
	}

	public static function Delete($id) {
		$ch = mysqli_fetch_object(Database::Query("SELECT * FROM docs_templates WHERE id='$id'"));

		if ($ch->status==0) {
			Database::Query("DELETE FROM docs_templates WHERE id='$id'");
		}
	}

	public static function Store($data) {
		//$data = Validator::Tem($data);
		$parent = $data['parent'];
		unset($data['parent']);
		// var_dump($data); die();
		if ($data['title'] == "") return false;
		$mq = Database::InsertI("docs_templates", $data);
		$idd = mysqli_insert_id($mq);
		// var_dump($idd); die();
			if ($parent!=0) {
				self::copyParts($parent, $idd);
			}
			return true;
		
		return false;
	}

	public static function copyParts($from, $to) {
		$all = mysqli_fetch_all(Database::Query("SELECT * FROM parts WHERE template_id='$from' ORDER BY id"));

		if(count($all)>0) {
			foreach ($all as $p) {
				$data = ['template_id'=>$to, 'title'=>$p[2], 'document'=>$p[3]];
				$check_existing = mysqli_fetch_all(Database::Query("SELECT * FROM parts WHERE template_id='$to' AND title='$p[2]'"));
				if (count($check_existing < 1)) Database::Insert("parts", $data);
			}
		}
	}

	public static function GetAll() {
		if ($temps = Database::Query("Select * FROM docs_templates WHERE user_id='$_SESSION[id]'")) {
			$temps = mysqli_fetch_all($temps);
			$res = [];
			foreach ($temps as $temp) {
				$res[] = new self($temp[0]);
			}
			return $res;
		}
		return false;
	}

	public static function GetAllConfirmed() {
		if ($temps = Database::Query("Select * FROM docs_templates WHERE status='1'")) {
			$temps = mysqli_fetch_all($temps);
			$res = [];
			foreach ($temps as $temp) {
				$res[] = new self($temp[0]);
			}
			return $res;
		}
		return false;
	}

	public static function GetAllAsObjects() {
		if ($temps = Database::Query("Select * FROM docs_templates")) {
			$tids = mysqli_fetch_all($temps);
			$res = [];
			foreach ($tids as $t) {
				$res[] = new Templates($t[0]);
			}
			return $res;
		}
		return false;
	}

	public static function Update($data, $id) {
		if (Database::Update("docs_templates", $data, ['id'=>$id])){
			echo "ok";
			// exit();
		}
		else {
			echo "No!<br>";
			print_r($data);
			// exit();
		}
		
	}

	public static function Updateinfo($id, $data) {
		Database::Update("docs_templates", $data, ['id'=>$id]);
		return true;
	}

	public static function Parse($text) {
		// $text = htmlspecialchars($text);
		$text = preg_replace("/\[n\]/", "[n;]", $text);
		$text = preg_replace("/\[t\]/", "[t;]", $text);
		$text = preg_replace("/\[l\]/", "[l;]", $text);
		preg_match_all("/\[[a-z];[0-9]{1,}\]/", $text, $out);
		preg_match_all("/\[[a-z];[0-9]{1,};.*?\]/", $text, $out2);
		$out = array_diff($out[0], $out2[0]);
		$res = [];
		foreach ($out as $s) {
			$res[] = $s;
		}
		$out = $res;
		for ($i=0; $i<count($out); $i++) {
			$j = $i;
			while (preg_match("/\[[a-z];$j;.*?\]/", $text)==1) $j++;
			$o = $out[$i];
			$o = preg_replace("/\[/", "", $o);
			$o = preg_replace("/\]/", "", $o);
			$o = preg_replace("/\(/", "\(", $o);
			$o = preg_replace("/\)/", "\)", $o);
			$n = explode(";", $o);
			if (preg_match("/\[[a-z];[0-9]{1,};.*?\]/", $out[$i]) == 0) $text = preg_replace("/\[$o\]/", "[".$n[0].";$j;".$n[1]."]", $text, 1);
		}
		// $text = preg_replace("/\\\(/", "(", $text);
		// $text = preg_replace("/\\\)/", ")", $text);
		return $text;
	}

	public static function ParseOut($text) {
		preg_match_all("/\[[a-z];[0-9]{1,};\]/", $text, $out);
		// dd($out[0]);
		foreach ($out[0] as $o) {
			$o = preg_replace("/\[/", "", $o);
			$o = preg_replace("/\]/", "", $o);
			$o = preg_replace("/\(/", "\(", $o);
			$o = preg_replace("/\)/", "\)", $o);
			$n = explode(";", $o);
			$n[2] = stripslashes($n[2]);
			if ($n[0] == "l") {
				$text = preg_replace("/\[$o\]/", "<textarea class='$n[0] ckeditor' name='$n[0]$n[1]' placeholder='$n[2]'></textarea>", $text);
			}
			else {
				$text = preg_replace("/\[$o\]/", "<input type='text' class='$n[0]' name='$n[0]$n[1]' placeholder='$n[2]'>", $text);
			}
		}
		// $text = preg_replace("/\[opopmanager]/", "[Руководитель ОП]", $text);
		// $text = preg_replace("/\[direction]/", "[Направление подготовки]", $text);
		// $text = preg_replace("/\[direction]/", "[Направление подготовки]", $text);
		// $text = preg_replace("/\[profile]/", "[Профиль]", $text);
		// $text = preg_replace("/\[studytype]/", "[Форма обучения]", $text);
		// $text = preg_replace("/\[facultymanager]/", "[Декан]", $text);
		// $text = preg_replace("/\\\)/", "\)", $text);
		return $text;
	}

	public static function Status($id) {
		$tmp = self::Get($id);
		if ($tmp->status == 0) {
			Database::Update("docs_templates", ['status'=>1], ['id'=>$id]);
		}
		elseif ($tmp->status == 1) {
			Database::Update("docs_templates", ['status'=>0], ['id'=>$id]);
		}
		else {
			return;
		}
	}

}

?>