<?php

class User extends Models {

	public $id;
	public $name;
	public $surname;
	public $lastname;
	public $email;
	public $phone;
	public $login;
	public $pulpit_id;
	public $faculty_id;
	public $opop_manager;
	public $sc_title;
	public $sc_level;
	public $roles;
	public $deleted;


	public function __construct($id) {
		
		if ($user = self::Get($id)) {
			unset($user->password);
			@[$this->id,
			$this->login,
			$this->name,
			$this->surname,
			$this->lastname,
			$this->email,
			$this->phone,
			$this->pulpit_id,
			$this->faculty_id,
			$this->opop_manager,
			$this->sc_title,
			$this->sc_level,
			$this->deleted,
			$this->roles] = array_values((array)$user);

			@$this->management = mysqli_fetch_all(Database::Query("Select * from opop_managers WHERE user_id='$this->id'"));
		}

		return false;
	}

	public static function Get($id) {

		if ($query = Database::Query("SELECT * FROM users WHERE id = $id AND deleted='0'")) {
			$r = [];
			$roles = array_values(mysqli_fetch_all(Database::Query("SELECT role_id FROM roles_users WHERE user_id = $id")));
			foreach ($roles as $rol) {
				$r[] = $rol[0];
			}
			$query = mysqli_fetch_object($query);
			// dd($roles);
			@$query->roles = $r;
			return $query;
		}

		return false; 
	}

	public static function Create($data) {
		$roles = $data['role'];

		unset($data['role']);

		$opops = [];

		if (isset($data['opop_faculty_id']) && count($data['opop_faculty_id']) > 0) {
			for ($i=0; $i < count($data['opop_faculty_id']); $i++) { 
				$opops[] = ["faculty_id"	=>	$data['opop_faculty_id'][$i], 
							"pulpit_id"		=>	$data['opop_pulpit_id'][$i], 
							"direction_id"	=>	$data['opop_direction_id'][$i], 
							"profile_id"	=>	$data['opop_profile_id'][$i],
							"study_type"	=>	$data['opop_study_type'][$i]];
			}
			unset($data['opop_faculty_id']);
			unset($data['opop_pulpit_id']);
			unset($data['opop_direction_id']);
			unset($data['opop_profile_id']);
			unset($data['opop_study_type']);

		}

		$data['password'] = md5($data['password']);
		if(Database::Insert("users", $data)){
			$uid = mysqli_fetch_all(Database::Query("SELECT * FROM users WHERE login='".$data['login']."'"))[0][0];

			Database::Insert("posts_users", ["user_id"=>$uid, "post_id"=>$post]);

			foreach ($roles as $role) {
				Database::Insert("roles_users", ["user_id"=>$uid, "role_id"=>$role]);
			}

			foreach ($opops as $opop) {
				$opop['user_id'] = $uid;
				Database::Insert("opop_managers", $opop);
			}
			return true;
		}

		return false;
	}

	public static function Update($data, $id = 0 ) {

		// dd($data);
		$roles = $data['role'];
		unset($data['role']);

		$opops = [];

		if (isset($data['opop_faculty_id']) && count($data['opop_faculty_id']) > 0) {
			for ($i=0; $i < count($data['opop_faculty_id']); $i++) { 
				$opops[] = ["faculty_id"	=>	$data['opop_faculty_id'][$i], 
							"pulpit_id"		=>	$data['opop_pulpit_id'][$i], 
							"direction_id"	=>	$data['opop_direction_id'][$i], 
							"profile_id"	=>	$data['opop_profile_id'][$i],
							"study_type"	=>	$data['opop_study_type'][$i]];
			}
			unset($data['opop_faculty_id']);
			unset($data['opop_pulpit_id']);
			unset($data['opop_direction_id']);
			unset($data['opop_profile_id']);
			unset($data['opop_study_type']);

		}

		if (!empty($data['password']) && $data['password']!="") {
			$data['password'] = md5($data['password']);
		}
		else {
			unset($data['password']);
		}

		if(Database::Update("users", $data, ['id'=>$id])){
			$uid = $id;

			// Database::Query("DELETE FROM posts_users WHERE user_id='$id'");
			Database::Query("DELETE FROM roles_users WHERE user_id='$id'");
			Database::Query("DELETE FROM opop_managers WHERE user_id='$id'");
			// Database::Insert("posts_users", ["user_id"=>$uid, "post_id"=>$post]);


			foreach ($roles as $role) {
				Database::Insert("roles_users", ["user_id"=>$uid, "role_id"=>$role]);
			}

			foreach ($opops as $opop) {
				$opop['user_id'] = $uid;
				if ($opop['profile_id'] == 0) unset($opop['profile_id']);
				Database::Insert("opop_managers", $opop);
			}
			return true;
		}

		return false;
	}

	public static function SaveAvatar($file) {
		if ($file['type'] != "image/png" && $file['type'] != "image/jpeg" && $file['type'] != "image/gif")
			exit();

		$dir = "Files/Avatars/";
		$newname = $dir.time().".jpg";
		move_uploaded_file($file['tmp_name'], $newname);
		$user = self::Get($_SESSION['id']);
		if ($user->avatar != "Files/Avatars/default.jpg") {
			unlink($user->avatar);
		}
		Database::Update("users", ['avatar'=>$newname], ['id'=>$_SESSION['id']]);
		header("Location: /");
	}

	public static function Auth($data=''){
		if($data == '') $data = Request::post(); 

		//var_dump($data);
		$login = $data->login;
		$password = md5($data->password);

		if ($query = Database::Query("SELECT * FROM users WHERE login = '$login' AND password = '$password' AND deleted='0'")) {
			$res = mysqli_fetch_object($query);
			$_SESSION['id'] = $res->id;
			$_SESSION['hash'] = $res->login;
			return true;
		} 

		return false;
	}

	public static function GetAll() {
		return mysqli_fetch_all(Database::Query("SELECT * FROM users WHERE deleted=0"));
	}

	public static function GetTable($role = null) {
		if($users = Database::Query("SELECT users.id, surname, name, lastname, faculties.title from users, faculties where users.faculty_id = faculties.id AND deleted=0")) {
			return mysqli_fetch_all($users);
		}
		return false;
	}

	public static function Delete($id) {
		Database::Query("UPDATE users SET deleted=1 WHERE id='$id'");
		return;
	}

}

?>