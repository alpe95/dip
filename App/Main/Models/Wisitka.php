<?php

require_once("App\Main\System\Database.php");
require_once("App\Main\Models\User.php");

class Wisitka extends Models {

	public $id;
	public $title;
	public $description;
	public $phone;
	public $email;
	public $image;
	public $user;
	public $theme;

	public function __construct($id) {
		$wis = self::Get($id);
		[$this->id, 
		$this->title, 
		$this->description, 
		$this->phone, 
		$this->email, 
		$this->image, 
		$this->user, 
		$this->theme] = objectToArray($wis);
	}

	public static function Get($id) {
		if ($query = Database::Query("SELECT * FROM wis WHERE id = $id")) {
			return mysqli_fetch_object($query);
		}
		return false; 
	}

	public static function Update($data, $id ) {
		if($data = Validator::WisSave($data)) {
			if(self::Get($id)->user != $_SESSION['id']) {
				echo "Access denied!";
				return;
			}
			if (Database::Update("wis", $data, ['id'=>$id])){
				echo "ok";
			}
			else {
				echo "FUCK!<br>";
				print_r($data);
				// exit();
			}
		}
	}

	public static function SaveImage($file, $id) {
		if ($file['type'] != "image/png" && $file['type'] != "image/jpeg" && $file['type'] != "image/gif")
			exit();

		$dir = "Files/WisImages/";
		$newname = $dir.time().".jpg";
		move_uploaded_file($file['tmp_name'], $newname);
		$wis = self::Get($_SESSION['id'], $id);
		if ($user->avatar != "Files/WisImages/default.jpg") {
			unlink($user->avatar);
		}
		return $newname;
	}

	public static function GetAll() {
		$res = mysqli_fetch_all(Database::Query("SELECT * FROM wis WHERE user='$_SESSION[id]'"));
		if (count($res) > 0) {
			return $res;
		}
		else {
			return false;
		}
	}

}