<?php

class Controllers {

	public function ShowView($Path, $Temp, $Data = []) {		
		if(isset($_GET['ajax'])) {
			self::ShowViewNoTemplate($Path, $Temp, $Data);
		}
		else {
			extract($Data);
			$view = "App/Main/Views/".$Path."/".$Temp.".php";
			$content_view = ("App/Main/Views/Template.php");	
			include ("App/Main/System/ViewIni.php");
		}
	}

	public function ShowViewNoTemplate($Path='', $Temp, $Data = []) {
		extract($Data);
		$content_view = "App/Main/Views/".$Path."/".$Temp.".php";
		include ("App/Main/System/ViewIni.php");
		
	}

	public function ShowPage($Page, $Data=[]) {
		extract($Data);
		$content_view = "App/Main/Views/".$Page;
		include ("App/Main/System/ViewIni.php");
	}

	public function CheckAccess($role) {
		global $Me;
		// var_dump($Me->Profile->roles);
		$roles = explode(",", $role);
		// var_dump($roles); die();
		$counter = 0;
		foreach ($roles as $r) {
			if($r!="" && in_array($r, $Me->Profile->roles)) {
				$counter++;
			}
		}
		if ($counter == 0) {
			echo "Недостаточно прав.";
			exit();
		}
	}

}
 
?>