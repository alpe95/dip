<ol class="breadcrumb">
	<li class="breadcrumb-item"><a href="/documents/new">Редактирование документа</a></li>
	<li class="breadcrumb-item active"><?=$document->title?> - Редактирование</li>
</ol>

<form action="/documents/update/<?=$_GET['id']?>" method="POST">
	<input class="form-control" type="text" name="title" placeholder="Название документа" required="true" value="<?=$document->title?>">
	<div style="padding: 10px;" class="showw">
		<?=$document->document?>
	</div>
	<div class="form-group">
		<input class="form-control btn btn-success" type="submit" value="Сохранить">
	</div>
</form>