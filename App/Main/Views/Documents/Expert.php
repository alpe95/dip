<ol class="breadcrumb">
  <li class="breadcrumb-item active">Документы для проверки</li>
</ol>
<div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Список Документов </div>
        <div class="card-body">
          <div class="table-responsive">
            <?php if ($documents) { ?>
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Название</th>
                  <th>Шаблон</th>
                  <th>Статус</th>
                  <th>Просмотреть</th>
                  <!-- <th>Редактировать</th> -->
                  <th>Проверка</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>Название</th>
                  <th>Шаблон</th>
                  <th>Статус</th>
                  <th>Просмотреть</th>
                  <!-- <th>Редактировать</th> -->
                  <th>Проверка</th>
                </tr>
              </tfoot>
              <tbody>
                <?php foreach($documents as $u) { ?>
                  <tr>
                    <td><?=$u[1]?></td>
                    <td><?=$u[3]?></td>
                    <td><?=$u[4]?></td>
                    <td><a href="/documents/show/<?=$u[0]?>" onclick="go(this); return false;" class="btn btn-warning" style="color: #fff;">Просмотр</a></td>
<!-- 
                    <?php if($u[4] == 1) { ?>
                    <td><a href="/documents/edit/<?=$u[0]?>"  class="btn btn-success" style="color: #fff;">Изменить</a></td>
                    <?php } else { ?>
                    <td><a href="/documents/edit/<?=$u[0]?>" onclick="return false;" class="btn btn-default" style="color: #aaa;" style="color: #fff;">Изменить</a></td>
                    <?php } ?> -->

                    
                    <?php if($u[6] == 1) { ?>
                    <td><a href="/documents/decision/<?=$u[0]?>" o nclick="go(this); return false;" class="btn btn-success" style="color: #fff;">Отправить на проверку</a></td>
                    <?php } elseif ($u[6] == 2) { ?>
                    <td><a href="/documents/decision/<?=$u[0]?>" onc lick="go(this); return false;" class="btn btn-success" style="color: #fff;">Одобрить/Отклонить</a></td>
                    <?php } else { ?>
                    <td>-</td>
                    <?php } ?>
                 
                  </tr>
                <?php } ?>
              </tbody>
            </table>
            <?php } ?>
          </div>
        </div>