<ol class="breadcrumb">
	<li class="breadcrumb-item"><a href="/documents/expert">Документы на проверку</a></li> <li class="breadcrumb-item">Решение</li>
</ol>

<form action="/documents/exsave/<?=$_GET['id']?>" method="POST">

	<div class="form-group">
		<select name="status" class="form-control">
			<option value="1">Отклонить</option>
			<option value="3">Одобрить</option>
		</select>
	</div>
	<div class="form-group">
		<textarea class="form-control" name="comment" placeholder="Комментарий"></textarea>
	</div>
	<div class="form-group">
		<input class="form-control btn btn-success" type="submit" value="Отправить">
	</div>
</form>