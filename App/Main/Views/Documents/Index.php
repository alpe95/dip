<ol class="breadcrumb">
  <li class="breadcrumb-item active">Документы</li>
</ol>
<div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Список Документов <a href="/documents/new" class="btn btn-primary" style="float: right; color: #fff;" onclick="go(this); return false;">Создать документ</a></div>
        <div class="card-body">
          <div class="table-responsive">
            <?php if ($documents) { ?>
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Название</th>
                  <th>Год</th>
                  <!-- <th>Шаблон</th> -->
                  <th>Просмотреть</th>
                  <th>Редактировать</th>
                  <th>Статус</th>
                  <th>Удалить</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>Название</th>
                  <th>Год</th>
                  <!-- <th>Шаблон</th> -->
                  <th>Просмотреть</th>
                  <th>Редактировать</th>
                  <th>Статус</th>
                  <th>Удалить</th>
                </tr>
              </tfoot>
              <tbody>
                <?php foreach($documents as $u) { ?>
                  <tr>
                    <td><?=$u[1]?> <font color="#bebebe"><?=$u[11]?></font></td>
                    <td><?=$u[4]?></td>
                    <!-- <td><?=$u[3]?></td> -->
                    <td><a href="/documents/show/<?=$u[0]?>" onclick="go(this); return false;" class="btn btn-warning" style="color: #fff;">Просмотр</a></td>

                    <?php if($u[6] == 1) { ?>
                    <td><a href="/documents/edit/<?=$u[0]?>"  class="btn btn-success" style="color: #fff;">Изменить</a></td>
                    <?php } else { ?>
                    <td><a href="/documents/edit/<?=$u[0]?>" onclick="return false;" class="btn btn-default" style="color: #aaa;" style="color: #fff;">Изменить</a></td>
                    <?php } ?>

                    
                    <?php if($u[6] == 1) { ?>
                    <td><a href="/documents/check/<?=$u[0]?>" o nclick="go(this); return false;" class="btn btn-success" style="color: #fff;">Черновик (Утвердить)</a></td>
                    <?php } elseif ($u[6] == 2) { ?>
                    <td><a href="/documents/check/<?=$u[0]?>" onc lick="go(this); return false;" class="btn btn-danger" style="color: #fff;">Снять с проверки</a></td>
                    <?php } else { ?>
                    <td><a href="/documents/check/<?=$u[0]?>" onc lick="go(this); return false;" class="btn btn-warning" style="color: #fff;" onclick="if(confirm('Отозвать из оборота <?=$u[1]?>?')) { return true; }; return false;">Утвержден (Отозвать)</a></td>
                    <?php } ?>
                    <td><a href="/documents/delete/<?=$u[0]?>" onclick="if(confirm('Удалить <?=$u[1]?>?')) { return true; }; return false;" class="btn btn-danger" style="color: #fff;">Удалить</a></td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
            <?php } ?>
          </div>
        </div>