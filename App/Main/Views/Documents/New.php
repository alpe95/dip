<ol class="breadcrumb">
	<li class="breadcrumb-item"><a href="/documents/new">Создание документа</a></li>
	<li class="breadcrumb-item active"><?=$template->title?> - Создание</li>
</ol>

<form action="/documents/store/<?=$_GET['id']?>" method="POST">

	<div class="form-group">
		<label>Название</label>
		<input class="form-control" type="text" name="title" placeholder="Название документа" required="true">
	</div>

	<div class="form-group">
		<label>Год приема</label>
		<select class="form-control" name="accepted_year">
			<option>2014</option>
			<option>2015</option>
			<option>2016</option>
			<option>2017</option>
			<option>2018</option>
			<option>2019</option>
			<option>2020</option>
			<option>2021</option>
			<option>2022</option>
			<option>2023</option>
			<option>2024</option>
			<option>2025</option>
		</select>
	</div>

	<div class="form-group">
		<label>Факультет</label>
		<select class="form-control" name="faculty_id" id="faculty_id" placeholder="Факультет" onchange="loadForm(this, 'pulpit_id', 'getpulpits'); loadForm(this, 'direction_id', 'getdirections');">
			<option disabled selected>Выберите...</option>
			<?php 
				$faculties = mysqli_fetch_all(Database::Query("SELECT * FROM faculties"));
				foreach ($faculties as $p) { ?>
				<option value="<?=$p[0]?>"><?=$p[1]?></option>
			<?php } ?>
		</select>
	</div>

	<div class="form-group">
		<label>Кафедра</label>
		<select class="form-control" name="pulpit_id" id="pulpit_id" placeholder="Кафедра" onchange="">

		</select>
	</div>

	<div class="form-group">
		<label>Направление подготовки</label>
		<select class="form-control" name="direction_id" id="direction_id" placeholder="Направление" onchange="loadForm(this, 'opop_manager', 'getopopmanagers');">

		</select>
	</div>

	<div class="form-group">
		<label>Уровень образования</label>
		<select class="form-control" name="study_level" id="study_level" placeholder="Уровень образования" onchange="loadFormNew(this, 'profile_id', 'getprofiles');">
			<option value="0" selected disabled>Не выбран</option>
			<?php 
				$experts = mysqli_fetch_all(Database::Query("SELECT * FROM study_levels"));
				foreach ($experts as $p) { ?>
				<option value="<?=$p[0]?>"><?=$p[1]?></option>
			<?php } ?>
		</select>
	</div>

	<div class="form-group">
		<label>Профиль</label>
		<select class="form-control" name="profile_id" id="profile_id" placeholder="Профиль">

		</select>
	</div>

	<div class="form-group">
		<label>Тип обучения</label>
		<select class="form-control" name="study_type" id="study_type" placeholder="Тип обучения">
			<?php 
				$experts = mysqli_fetch_all(Database::Query("SELECT * FROM study_types"));
				foreach ($experts as $p) { ?>
				<option value="<?=$p[0]?>"><?=$p[1]?></option>
			<?php } ?>
		</select>
	</div>

	<div class="form-group">
		<label>Руководитель ОП</label>
		<select class="form-control" name="opop_manager" id="opop_manager" placeholder="Руководитель ОП">

		</select>
	</div>

	

	<div class="form-group">
		<input class="form-control btn btn-success" type="submit" value="Создать">
	</div>
</form>