  <ol class="breadcrumb">
  <li class="breadcrumb-item active"><a href='/documents'>Документы</a> / <?=$doc->title?> / Выбор раздела</li>
</ol>
<div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Список разделов <!-- <a href="/documents/subjects/<?=$_GET['id']?>" class="btn btn-primary" style="float: right; color: #fff;" onclick="go(this); return false;">Дисциплины</a></font> <a href="/documents/matrix/<?=$_GET['id']?>" class="btn btn-primary" style="float: right; color: #fff;" onclick="go(this); return false;">Матрица</a>  --></div>
        <div class="card-body">
          <div class="table-responsive">
            <?php if ($parts) { ?>
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>№</th>
                  <th>Название</th>
                  <th>Редактировать</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>№</th>
                  <th>Название</th>
                  <th>Редактировать</th>
                </tr>
              </tfoot>
              <tbody>
                <?php $i=1; foreach($parts as $u) { ?>
                  <tr>
                    <td><?=$i?></td>
                    
                    <?php if (isset($_GET['last']) && $_GET['last'] == $u[0]) { ?>
                      <td style="background: #fdfdd5;"><?=$u[2]?></td>
                    <?php } else { ?>
                      <td><?=$u[2]?></td>
                    <?php } ?>

                    <td><a href="/documents/edit/<?=$doc->id?>/?part=<?=$u[0]?>" onclick="go(this); return false;" class="btn btn-success" style="color: #fff;">Изменить</a></td>
                   
                  </tr>
                <?php $i++; } ?>
              </tbody>
            </table>
            <?php } ?>
          </div>
        </div>