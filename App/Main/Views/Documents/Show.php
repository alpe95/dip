<ol class="breadcrumb">
	<li class="breadcrumb-item"><a href="/documents">Документы</a></li>
	<li class="breadcrumb-item active"><?=$document->title?> - Просмотр</li>
	<?php if($document->status == 3) { ?>
		<a href="/documents/export/<?=$_GET['id']?>" style='float: right;' class='btn btn-success'>Экспорт в PDF</a>
		<a href="/documents/check/<?=$document->id?>?redirect=show" class="btn btn-warning" style='float: right; color: #fff'>Отозвать</a>
	<?php } else { ?>
		<a href="/documents/check/<?=$document->id?>?redirect=show" class="btn btn-success" style='float: right; color: #fff;'>Утвердить</a>
	<?php } ?>
</ol>

<div style="padding: 10px;" class="showw">
	<?=$document->document?>
</div>