<ol class="breadcrumb">
  <li class="breadcrumb-item active">Выберите шаблон</li>
</ol>
<div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Список Шаблонов</div>
        <div class="card-body">
          <div class="table-responsive">
            <?php if ($templates) { ?>
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Название</th>
                  <th>Уровень образования</th>
                  <th>Создал</th>
                  <th>Выбрать</th>

                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>Название</th>
                  <th>Уровень образования</th>
                  <th>Создал</th>
                  <th>Выбрать</th>

                </tr>
              </tfoot>
              <tbody>
                <?php foreach($templates as $u) { ?>
                  <tr>
                    <td><?=$u->title?></td>
                    <td><?=$u->study_level?></td>
                    <td><?=$u->user_info->surname?></td>
                    <td><a href="/documents/new/<?=$u->id?>" onclick="go(this); return false;" class="btn btn-success" style="color: #fff;">Выбрать</a></td>
              
                  </tr>
                <?php } ?>
              </tbody>
            </table>
            <?php } ?>
          </div>
        </div>