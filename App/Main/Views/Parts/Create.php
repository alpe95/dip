<ol class="breadcrumb">
	<li class="breadcrumb-item"><a href="/templates/edit/<?=Request::get()->id?>">Разделы шаблона</a></li>
	<li class="breadcrumb-item active">Создать</li>
</ol>
<!-- <script type="text/javascript" src="/js/nicEdit.js"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
</script> -->

<script src="/ckeditor/ckeditor.js"></script>
<form action="/parts/store/<?=Request::get()->id?>" method="POST">
	<div class="form-group">
		<textarea id='editor' name="document" style="width: 100%; height: 400px;">
			
		</textarea>
		<script type="text/javascript">
			CKEDITOR.replace('editor');
		</script>
	</div>
	<div class="form-group">
		<label>Название</label>
		<input class="form-control" type="text" name="title" placeholder="Название">
	</div>
	<!-- <div class="form-group">
		<label><input type="checkbox" name="for_teachers"> Заполняется для каждой дисциплины</label>
		
	</div> -->
	<div class="form-group">
		<input class="form-control btn btn-success" type="submit" value="Создать">
	</div>
</form>