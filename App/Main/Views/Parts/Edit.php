<ol class="breadcrumb">
	<li class="breadcrumb-item"><a href="/templates/edit/<?=$part->template_id?>?last=<?=$_GET['id']?>">Разделы шаблона</a></li>
	<li class="breadcrumb-item active"><?=$part->title?> - Изменить</li>
</ol>
<!-- <script type="text/javascript" src="/js/nicEdit.js"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
</script> -->

<script src="/ckeditor/ckeditor.js"></script>
<form action="/parts/update/<?=$part->id?>" method="POST">
	<div class="form-group">
		<textarea id='editor' name="document" style="width: 100%; height: 400px;"><?=$part->document?></textarea>
		<script type="text/javascript">
			CKEDITOR.replace('editor');
		</script>
	</div>
	<div class="form-group">
		<label>Название</label>
		<input class="form-control" type="text" name="title" placeholder="Название" value="<?=$part->title?>">
	</div>
	<div class="form-group">
		<input class="form-control btn btn-success" type="submit" value="Сохранить">
	</div>
</form>