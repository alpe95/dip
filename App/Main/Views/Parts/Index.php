<ol class="breadcrumb">
  <li class="breadcrumb-item active">Шаблоны документов</li>
</ol>
<div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Список Шаблонов <a href="/templates/new" class="btn btn-primary" style="float: right; color: #fff;" onclick="go(this); return false;">Создать шаблон</a></div>
        <div class="card-body">
          <div class="table-responsive">
            <?php if ($templates) { ?>
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Название</th>
                  <th>Год приема</th>
                  <th>Создал</th>
                  <th>Просмотреть</th>
                  <th>Редактировать</th>
                  <th>Статус</th>
                  <th>Удалить</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>Название</th>
                  <th>Год приема</th>
                  <th>Создал</th>
                  <th>Просмотреть</th>
                  <th>Редактировать</th>
                  <th>Статус</th>
                  <th>Удалить</th>
                </tr>
              </tfoot>
              <tbody>
                <?php foreach($templates as $u) { ?>
                  <tr>
                    <td><?=$u->title?></td>
                    <td><?=$u->accepted_year?></td>
                    <td><?=$u->user?></td>
                    <td><a href="/templates/show/<?=$u->id?>" onclick="go(this); return false;" class="btn btn-warning" style="color: #fff;">Просмотр</a></td>
                    <td><a href="/templates/edit/<?=$u->id?>" onclick="go(this); return false;" class="btn btn-success" style="color: #fff;">Изменить</a></td>
                     <td><a href="/templates/status/<?=$u->id?>" class="btn btn-success" style="color: #fff;"><?=$u->stat_text[$u->status]?></a></td>
                    <td><a href="/templates/delete/<?=$u->id?>" onclick="if(confirm('Удалить <?=$u->title?>?')) { return true; }; return false;" class="btn btn-danger" style="color: #fff;">Удалить</a></td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
            <?php } ?>
          </div>
        </div>