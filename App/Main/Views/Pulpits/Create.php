<ol class="breadcrumb">
	<li class="breadcrumb-item"><a href="/pulpits">Факультеты</a></li>
	<li class="breadcrumb-item active">Создание факультета</li>
</ol>

<div class="alert alert-success hidden" style="display: none;" id="success-alert">
	Сохранено
</div>
<div class="alert alert-warning hidden" style="display: none;" id="warning-alert">
	Ошибка: 
</div>
<form role="form" action="/users/store?type=<?=$_GET['type']?>" method="POST" ons ubmit="SendXHR(this); return false;">
	<div class="form-group">
		<label>Имя</label>
		<input required="required" autocomplete="off"  class="form-control" type="text" name="name" placeholder="Имя">
	</div>
	<div class="form-group">
		<label>Фамилия</label>
		<input required="required" autocomplete="off"  class="form-control" type="text" name="surname" placeholder="Фамилия">
	</div>
	<div class="form-group">
		<label>Логин</label>
		<input required="required" autocomplete="off"  class="form-control" type="text" name="login" placeholder="Логин" value="<?=$login?>">
	</div>

	<div class="form-group">
		<label>Пароль (временный)</label>
		<input required="required" autocomplete="off"  class="form-control" type="text" name="password" placeholder="Пароль" value="<?=$login?>">
	</div>

	<div class="form-group">
		<label>E-mail</label>
		<input required="required" autocomplete="off"  class="form-control" type="text" name="email" placeholder="Электронная почта">
	</div>

	<div class="form-group">
		<label>Телефон</label>
		<input required="required" autocomplete="off"  class="form-control" type="text" name="phone" placeholder="Номер телефона">
	</div>

	<div class="form-group">
		<label>Факультет</label>
		<select class="form-control" name="pulpit" placeholder="Факультет">
			<?php foreach ($pulpits as $p) { ?>
				<option value="<?=$p[0]?>"><?=$p[1]?></option>
			<?php } ?>
		</select>
	</div>

	<div class="form-group">
		<input required="required" autocomplete="off"  class="form-control btn btn-success" type="submit" value="Создать">
	</div>
</form>
