<ol class="breadcrumb">
  <li class="breadcrumb-item active">Настройки</li>
</ol>

<div class="alert alert-success hidden" style="display: none;" id="success-alert">
	Сохранено
</div>
<div class="alert alert-warning hidden" style="display: none;" id="warning-alert">
	Ошибка: 
</div>
<form role="form" action="settings/save" method="POST" onsubmit="SendXHR(this); return false;">
	<div class="form-group">
		<label>Логин</label>
		<input class="form-control" type="text" name="login" placeholder="Логин" value="<?=$Me->Profile->login?>">
	</div>
	<div class="form-group">
		<label>Email</label>
		<input class="form-control" type="text" name="email" placeholder="Email" value="<?=$Me->Profile->email?>">
	</div>
	<div class="form-group">
		<label>Телефон</label>
		<input class="form-control" type="text" name="phone" placeholder="Телефон" value="<?=$Me->Profile->phone?>">
	</div>
	<div class="form-group">
		<label>Новый пароль (Оставьте поле пустым если не хотите менять пароль)</label>
		<input class="form-control" type="password" name="password" placeholder="Пароль">
	</div>
	<div class="form-group">
		<label>Подтверждение пароля</label>
		<input class="form-control" type="password" name="password-check" placeholder="Пароль еще раз">
	</div>
	<div class="form-group">
		<input class="form-control btn btn-success" type="submit" value="Сохранить">
	</div>
</form>
