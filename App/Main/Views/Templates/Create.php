<ol class="breadcrumb">
	<li class="breadcrumb-item"><a href="/templates">Шаблоны документов</a></li>
	<li class="breadcrumb-item active">Создать</li>
</ol>
<!-- <script type="text/javascript" src="/js/nicEdit.js"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
</script> -->

<script src="/ckeditor/ckeditor.js"></script>
<form action="/templates/store" method="POST">
	<div class="form-group">
		<label>Название</label>
		<input class="form-control" type="text" name="title" placeholder="Название" required="required">
	</div>
	
	<?php
		$slevel = mysqli_fetch_all(Database::Query("SELECT * FROM study_levels"));
		// var_dump($tmpls); die();
	?>

	<div class="form-group">
		<label>Уровень образования</label>
		<select name="study_level" class="form-control">
			<option selected value="0"> Не выбран</option>
			<?php
				foreach ($slevel as $t) {
					echo "<option value='$t[0]'>$t[1]</option>";
				}
			?>
		</select>
	</div>

	<?php
		$stype = mysqli_fetch_all(Database::Query("SELECT * FROM study_types"));
		// var_dump($tmpls); die();
	?>

	<!-- <div class="form-group">
		<label>Форма обучения</label>
		<select name="study_type" class="form-control">
			<option selected value="0"> Не выбран</option>
			<?php
				// foreach ($stype as $t) {
				// 	echo "<option value='$t[0]'>$t[1]</option>";
				// }
			?>
		</select>
	</div> -->

	<div class="form-group">
		<label>Примечание к шаблону</label>
		<textarea class="form-control" name="comment" placeholder="Примечание"></textarea>
	</div>

	<?php
		$tmpls = mysqli_fetch_all(Database::Query("SELECT * FROM docs_templates"));
		// var_dump($tmpls); die();
	?>

	<div class="form-group">
		<label>Скопировать разделы из шаблона</label>
		<select name="parent" class="form-control">
			<option selected value="0"> Не выбран</option>
			<?php
				foreach ($tmpls as $t) {
					echo "<option value='$t[0]'>$t[1]</option>";
				}
			?>
		</select>
	</div>

	<div class="form-group">
		<input class="form-control btn btn-success" type="submit" value="Создать">
	</div>
</form>

	

	