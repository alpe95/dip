 <ol class="breadcrumb">
  <li class="breadcrumb-item active"><a href="/templates">Все шаблоны</a> / Разделы шаблона <?=$template->title?></li>
</ol>
<div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Список разделов

           <a href="/parts/new/<?=$template->id?>" class="btn btn-primary" style="float: right; color: #fff;" onclick="go(this); return false;">Создать раздел</a>
          <a href="/templates/edittemplate/<?=$template->id?>" class="btn" style="float: right; color: #ссс;" onclick="go(this); return false;"><span class="glyphicon glyphicon-pencil">Редактировать</span></a>


         </div>
        <div class="card-body">
          <div class="table-responsive">
            <?php if ($parts) { ?>
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>№</th>
                  <th>Название</th>
                  <th>Редактировать</th>
                  <th>Удалить</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>№</th>
                  <th>Название</th>
                  <th>Редактировать</th>
                  <th>Удалить</th>
                </tr>
              </tfoot>
              <tbody>
                <?php $i=1; foreach($parts as $u) { ?>
                  <tr>
                    <td><?=$i?></td>

                    <?php if (isset($_GET['last']) && $_GET['last'] == $u[0]) { ?>
                      <td style="background: #fdfdd5;"><?=$u[2]?></td>
                    <?php } else { ?>
                      <td><?=$u[2]?></td>
                    <?php } ?>

                    <td><a href="/parts/edit/<?=$u[0]?>" onclick="go(this); return false;" class="btn btn-success" style="color: #fff;">Изменить</a></td>
                    <td><a href="/parts/delete/<?=$u[0]?>" onclick="if(confirm('Удалить <?=$u[2]?>?')) { return true; }; return false;" class="btn btn-danger" style="color: #fff;">Удалить</a></td>
                  </tr>
                <?php $i++; } ?>
              </tbody>
            </table>
            <?php } ?>
          </div>
        </div>