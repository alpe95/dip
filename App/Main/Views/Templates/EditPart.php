<ol class="breadcrumb">
	<li class="breadcrumb-item"><a href="/templates">Шаблоны документов</a></li>
	<li class="breadcrumb-item active"><?=$template->title?> - Изменить</li>
</ol>
<!-- <script type="text/javascript" src="/js/nicEdit.js"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
</script> -->

<script src="/ckeditor/ckeditor.js"></script>
<form action="/templates/update/<?=$template->id?>" method="POST">
	<div class="form-group">
		<textarea id='editor' name="document" style="width: 100%; height: 400px;"><?=$template->document?></textarea>
		<script type="text/javascript">
			CKEDITOR.replace('editor');
		</script>
	</div>
	<div class="form-group">
		<label>Название</label>
		<input class="form-control" type="text" name="title" placeholder="Название" value="<?=$template->title?>">
	</div>
	<div class="form-group">
		<label>Год приема</label>
		<select class="form-control" name="accepted_year" placeholder="Год приема">
			<option selected="true" disabled="true"><?=$template->accepted_year?></option>
			<option>2016</option>
			<option>2017</option>
			<option>2018</option>
			<option>2019</option>
		</select>
	</div>
	<div class="form-group">
		<input class="form-control btn btn-success" type="submit" value="Сохранить">
	</div>
</form>