<ol class="breadcrumb">
	<li class="breadcrumb-item"><a href="/templates">Шаблоны документов</a></li>
	<li class="breadcrumb-item active">Изменить</li>
</ol>
<!-- <script type="text/javascript" src="/js/nicEdit.js"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
</script> -->

<script src="/ckeditor/ckeditor.js"></script>
<form action="/templates/updateinfo/<?=$template->id?>" method="POST">
	<div class="form-group">
		<label>Название</label>
		<input class="form-control" type="text" name="title" placeholder="Название" value="<?=$template->title?>">
	</div>

	<div class="form-group">
		<label>Примечание к шаблону</label>
		<textarea class="form-control" name="comment" placeholder="Примечание"><?=$template->comment?></textarea>
	</div>

	<div class="form-group">
		<input class="form-control btn btn-success" type="submit" value="Сохранить">
	</div>
</form>