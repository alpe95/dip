<ol class="breadcrumb">
  <li class="breadcrumb-item active">Шаблоны документов</li>
</ol>
<div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Список Шаблонов <a href="/templates/new" class="btn btn-primary" style="float: right; color: #fff;" onclick="go(this); return false;">Создать шаблон</a></div>
        <div class="card-body">
          <div class="table-responsive">
            <?php if ($templates) { ?>
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Название</th>
                  <th>Уровень образования</th>
                  

                  <th>Просмотреть</th>
                  <th>Редактировать</th>
                  <th>Статус</th>

                  <th>Изменен</th>
                  <th>Удалить</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>Название</th>
                  <th>Уровень образования</th>
                  

                  <th>Просмотреть</th>
                  <th>Редактировать</th>
                  <th>Статус</th>

                  <th>Изменен</th>
                  <th>Удалить</th>
                </tr>
              </tfoot>
              <tbody>
                <?php foreach($templates as $u) { ?>
                  <tr>
                    <?php if ($u->comment != "") { ?>
                      <td><?=$u->title?> <font color="#bebebe">(<?=$u->comment?>)</font></td>
                    <?php } else { ?>
                      <td><?=$u->title?></td>
                    <?php } ?>

                    <td><?=$u->study_level?></td>
                    
                    <td><a href="/templates/show/<?=$u->id?>" onclick="go(this); return false;" class="btn btn-warning" style="color: #fff;">Просмотр</a></td>
                    <td>
                      <?php if ($u->status == 0) { ?>
                      <a href="/templates/edit/<?=$u->id?>" onclick="go(this); return false;" class="btn btn-success" style="color: #fff;">Изменить</a>

                      <?php } else { ?>
                        <a class="btn btn-success" disabled style="background: #fff; color: #ccc; border: none;">Изменить</a>
                      <?php } ?>
                    </td>

                    <?php if ($u->status == 0) { ?>
                     <td><a href="/templates/status/<?=$u->id?>" class="btn btn-success" style="color: #fff;">Утвердить</a></td>
                    <?php } else { ?>
                      <td><a href="/templates/status/<?=$u->id?>" class="btn btn-warning" style="color: #fff;">Отозвать</a></td>
                    <?php } ?>

                     

                    <td><?=$u->updated?></td>
                    <td>
                      <?php if ($u->status == 0) { ?>
                      <a href="/templates/delete/<?=$u->id?>" onclick="if(confirm('Удалить <?=$u->title?>?')) { return true; }; return false;" class="btn btn-danger" style="color: #fff;">Удалить</a>
                      <?php } else { echo '<a class="btn btn-success" disabled style="background: #fff; color: #ccc; border: none;">Удалить</a>';} ?>
                    </td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
            <?php } ?>
          </div>
        </div>