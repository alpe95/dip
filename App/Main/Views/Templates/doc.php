ИСФОП
 Шаблоны документов
 Мои настройки
Выйти
Шаблоны документовСоздать
 Источник                         
                                
СтилиФорматированиеШрифтРазмер     

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><strong><span style="font-size:10.0pt">Приложение 1. &laquo;Макет ОПОП программ бакалавриата, специалитета, магистратуры&raquo; </span></strong></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="font-size:10.0pt">к<strong> </strong></span><span style="font-size:10.0pt">Положению о проектировании и разработке основных профессиональных образовательных программ высшего образования Астраханского государственного университета</span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:center">&nbsp;</p>

<p style="margin-left:0cm; margin-right:0cm; text-align:center"><span style="font-size:12pt">МИНИСТЕРСТВО ОБРАЗОВАНИЯ И НАУКИ РОССИЙСКОЙ ФЕДЕРАЦИИ</span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify">&nbsp;</p>

<p style="margin-left:0cm; margin-right:0cm; text-align:center"><span style="font-size:12pt"><span style="font-size:11.0pt">ФЕДЕРАЛЬНОЕ ГОСУДАРСТВЕННОЕ БЮДЖЕТНОЕ<br />
ОБРАЗОВАТЕЛЬНОЕ УЧРЕЖДЕНИЕ ВЫСШЕГО ОБРАЗОВАНИЯ </span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:center"><span style="font-size:12pt"><span style="font-size:11.0pt">&laquo;АСТРАХАНСКИЙ ГОСУДАРСТВЕННЫЙ УНИВЕРСИТЕТ&raquo;</span></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify">&nbsp;</p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify">&nbsp;</p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify">&nbsp;</p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify">&nbsp;</p>

<table cellspacing="0" class="Table" style="border-collapse:collapse; border:undefined">
	<tbody>
		<tr>
			<td style="vertical-align:top; width:245.8pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:center"><span style="font-size:12pt">&laquo;Утверждаю&raquo;</span></p>

			<p style="margin-left:0cm; margin-right:0cm; text-align:justify">&nbsp;</p>

			<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt">Первый проректор &ndash; проректор по основной деятельности [t]</span></p>

			<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt">&laquo;[n]&raquo; [t]&nbsp;20[n] г.</span></p>

			<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt">[t]</span></p>

			<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><em><span style="font-size:8.0pt">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; номер внутривузовской регистрации</span></em></span></p>
			</td>
			<td style="vertical-align:top; width:31.9pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:justify">&nbsp;</p>
			</td>
			<td style="vertical-align:top; width:213.9pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:center"><span style="font-size:12pt">&laquo;Согласовано&raquo;</span></p>

			<p style="margin-left:0cm; margin-right:0cm; text-align:justify">&nbsp;</p>

			<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt">Председатель Ученого совета факультета</span></p>

			<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt">[t]</span></p>

			<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt">&laquo;[n]&raquo; [t]&nbsp;20[n]&nbsp;г.</span></p>
			</td>
		</tr>
	</tbody>
</table>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify">&nbsp;</p>

<p style="margin-left:0cm; margin-right:0cm">&nbsp;</p>

<p style="margin-left:0cm; margin-right:0cm; text-align:center"><span style="font-size:12pt"><strong><span style="font-size:14.0pt">ОСНОВНАЯ ПРОФЕССИОНАЛЬНАЯ</span></strong></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:center"><span style="font-size:12pt"><strong><span style="font-size:14.0pt">ОБРАЗОВАТЕЛЬНАЯ ПРОГРАММА</span></strong></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:center"><span style="font-size:12pt"><strong><span style="font-size:14.0pt">ВЫСШЕГО ОБРАЗОВАНИЯ</span></strong></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:center">&nbsp;</p>

<table align="center" cellspacing="0" class="Table" style="border-collapse:collapse; border:undefined">
	<tbody>
		<tr>
			<td style="vertical-align:top; width:218.05pt">
			<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:12pt">Направление подготовки</span></p>
			</td>
			<td style="vertical-align:top; width:273.55pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:right"><span style="font-size:12pt">[t]</span></p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top; width:218.05pt">
			<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:12pt">Направленность (профиль) ОПОП</span></p>
			</td>
			<td style="vertical-align:top; width:273.55pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:right"><span style="font-size:12pt"><strong>Профиль/программа подготовки</strong> </span></p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top; width:218.05pt">
			<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:12pt">Квалификация (степень)</span></p>
			</td>
			<td style="text-align:right; vertical-align:top; width:273.55pt"><span style="font-size:12pt">[t]</span></td>
		</tr>
		<tr>
			<td style="vertical-align:top; width:218.05pt">
			<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:12pt">Форма обучения</span></p>
			</td>
			<td style="text-align:right; vertical-align:top; width:273.55pt"><span style="font-size:12pt">[t]</span></td>
		</tr>
		<tr>
			<td style="vertical-align:top; width:218.05pt">
			<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:12pt">Объем образовательной программы</span></p>
			</td>
			<td style="vertical-align:top; width:273.55pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:right"><span style="font-size:12pt"><strong>240/300/120 з.е&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></span></p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top; width:218.05pt">
			<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:12pt">Срок освоения</span></p>
			</td>
			<td style="vertical-align:top; width:273.55pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:right"><span style="font-size:12pt">[n]</span></p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top; width:218.05pt">
			<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:12pt">Временной ресурс:</span></p>
			</td>
			<td style="vertical-align:top; width:273.55pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:right">&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top; width:218.05pt">
			<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:12pt">всего</span></p>
			</td>
			<td style="vertical-align:top; width:273.55pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:right"><span style="font-size:12pt">[n]&nbsp;<strong>час.</strong></span></p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top; width:218.05pt">
			<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:12pt">контактная работа </span></p>
			</td>
			<td style="vertical-align:top; width:273.55pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:right"><span style="font-size:12pt">[n]&nbsp;<strong>час.</strong></span></p>
			</td>
		</tr>
		<tr>
			<td style="height:21.75pt; vertical-align:top; width:218.05pt">
			<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:12pt">самостоятельная работа</span></p>
			</td>
			<td style="height:21.75pt; vertical-align:top; width:273.55pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:right"><span style="font-size:12pt">[n]&nbsp;<strong>час.</strong></span></p>
			</td>
		</tr>
		<tr>
			<td style="height:17.65pt; vertical-align:top; width:218.05pt">
			<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:12pt">аудиторные занятия</span></p>
			</td>
			<td style="height:17.65pt; vertical-align:top; width:273.55pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:right"><span style="font-size:12pt">[n]&nbsp;<strong>час.</strong></span></p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top; width:218.05pt">
			<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:12pt">Государственная итоговая аттестация</span></p>
			</td>
			<td style="vertical-align:top; width:273.55pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:right"><span style="font-size:12pt"><strong>бакалаврская работа /дипломная работа (проект) / магистерская диссертация, </strong><em>&nbsp;</em><strong>государственный&nbsp; экзамен </strong></span></p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top; width:218.05pt">
			<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:12pt">Выпускающие подразделения</span></p>
			</td>
			<td style="vertical-align:top; width:273.55pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:right"><span style="font-size:12pt"><strong>Факультет(-ы), кафедра(-ы)</strong></span></p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top; width:218.05pt">
			<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:12pt">Декан факультета</span></p>
			</td>
			<td style="vertical-align:top; width:273.55pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:right"><span style="font-size:12pt">, , , </span></p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top; width:218.05pt">
			<p style="margin-left:0cm; margin-right:0cm"><span style="font-size:12pt">Руководитель ОПОП</span></p>
			</td>
			<td style="vertical-align:top; width:273.55pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:right"><span style="font-size:12pt">, , , </span></p>
			</td>
		</tr>
	</tbody>
</table>

<p style="margin-left:0cm; margin-right:0cm; text-align:center">&nbsp;</p>

<p style="margin-left:0cm; margin-right:0cm; text-align:center"><span style="font-size:12pt">Астрахань 20[n] г.</span></p>

<p>&nbsp;</p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><strong>1. Общие положения</strong></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><strong>1.1. Основная профессиональная образовательная программа (ОПОП) подготовки бакалавра/специалиста/магистра </strong></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt">Основная профессиональная образовательная программа (ОПОП) бакалавриата/специалитета/магистратуры, реализуемая ФГБОУ&nbsp;ВО &laquo;Астраханский государственный университет&raquo; по направлению подготовки <strong>.</strong></span></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>1.2. Нормативные документы для разработки программы бакалавриата/специалитета/магистратуры </strong></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><em>[Указывается список нормативных документов для разработки ОПОП]:</em></span></strong></p>

<ul>
	<li style="text-align:justify"><strong><span style="font-size:12pt">Федеральный закон от 29 декабря 2012&nbsp;года № 273-ФЗ &laquo;Об образовании в Российской Федерации&raquo;;</span></strong></li>
	<li style="text-align:justify"><strong><span style="font-size:12pt">Федеральный государственный образовательный стандарт по направлению подготовки (специальности)</span></strong><span style="font-size:12pt">[t]</span><strong><span style="font-size:12pt"> и уровню высшего образования</span></strong><span style="font-size:12pt">[t]</span><strong><span style="font-size:12pt">, утвержденный приказом Минобрнауки России от</span></strong><span style="font-size:12pt">[t]</span><strong><span style="font-size:12pt"> №</span></strong><span style="font-size:12pt">[n]&nbsp;</span><strong><span style="font-size:12pt">(далее &ndash; ФГОС ВО);</span></strong></li>
	<li style="text-align:justify"><strong><span style="font-size:12pt">Порядок организации и осуществления образовательной деятельности по образовательным программам высшего образования &ndash; программам бакалавриата, программам магистратуры, программам специалитета, утвержденный приказом приказом Минобрнауки России от 13 декабря 2013 года №1367 (далее &ndash; Порядок организации образовательной деятельности);</span></strong></li>
	<li style="text-align:justify"><strong><span style="font-size:12pt">Порядок проведения государственной итоговой аттестации по образовательным программам высшего образования &ndash; программам бакалавриата, программам специалитета и программам магистратуры, утвержденный приказом Минобрнауки России от 29 июня 2015 г. № 636;</span></strong></li>
	<li style="text-align:justify"><strong><span style="font-size:12pt">Положение о практике обучающихся, осваивающих основные профессиональные образовательные программы высшего образования, утвержденное приказом Минобрнауки России от 27 ноября 2015 г. № 1383;</span></strong></li>
	<li style="text-align:justify"><strong><span style="font-size:12pt">&hellip;</span></strong></li>
</ul>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>1.3. Общая характеристика ОПОП бакалавриата/специалитета/магистратуры</strong></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>1.3.1. Цель (миссия) ОПОП</strong></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt">ОПОП [t] имеет своей целью развитие у студентов личностных качеств, а также формирование общекультурных, общепрофессиональных и профессиональных компетенций в соответствии с требованиями ФГОС ВО по данному направлению подготовки.</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><em>[При этом формулировка целей ОПОП как в области воспитания, так и в области обучения даётся с учетом специфики конкретной ОПОП, характеристики групп обучающихся, а также особенностей научной школы университета и потребностей рынка труда].</em></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>1.3.2. Объем, сроки освоения ОПОП и общая трудоемкость ОПОП в ЗЕ (часах) </strong><em>[данный пункт составляется в соответствии с п. 3.3 ФГОС ВО]</em></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt">Объем программы составляет ХХХ зачетных единиц (з.е.) независимо от формы обучения, применяемых образовательных технологий, реализации программы несколькими организациями, осуществляющими образовательную деятельность с использованием сетевой формы, реализации обучения по индивидуальному учебному плану, в том числе ускоренному обучению. </span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt">Срок получения образования по ОПОП<em>:</em></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt">&nbsp;&ndash; в очной форме обучения в соответствии с ФГОС ВО по данному направлению, включая каникулы, предоставляемые после прохождения государственной итоговой аттестации, независимо от применяемых образовательных технологий, составляет Х года (лет). Объем программы бакалавриата/специалитета/магистратуры в очной форме обучения, реализуемых за один учебный год, составляет ХХ з.е.; <em>[указывается только для заявленной на титульном листе формы обучения]</em> </span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt">&nbsp;&ndash; в очно-заочной и заочной формах обучения, вне независимости от применяемых образовательных технологий, увеличивается не менее чем на Х месяцев и не более чем на Х год(а), по сравнению со сроком получения образования по очной форме обучения; <em>[указывается только для заявленной на титульном листе формы обучения]</em></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt">&nbsp;&ndash; при обучении по индивидуальному учебному плану вне зависимости от формы обучения устанавливается не более срока получения образования, установленного для соответствующей формы обучения, а при обучении по индивидуальному плану лиц с ограниченными возможностями здоровья может быть увеличен по их желанию не более чем на Х год по сравнению со сроком получения образования для соответствующей формы обучения. Объем программы бакалавриата/специалитета/магистратуры за один учебный год при обучении по индивидуальному учебному плану в любой форме обучения не может составлять более ХХ з.е.</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt">Общая трудоемкость включает все виды учебной деятельности.</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>1.4 Требования к уровню подготовки, необходимому для освоения ОПОП (к абитуриенту</strong>)</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt">Абитуриент должен иметь документ государственного образца .</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>2. Характеристика профессиональной деятельности выпускника</strong></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>2.1.</strong> <strong>Область профессиональной деятельности</strong> выпускников, освоивших программы <em>бакалавриата/специалитета/магистратуры</em>, вне зависимости от присваиваемой квалификации включает:</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><em>[данный пункт составляется в соответствии с п. 4.1ФГОС ВО]</em></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>2.2.</strong> <strong>Объектами профессиональной деятельности</strong> выпускников, освоивших программы <em>бакалавриата/специалитета/магистратуры</em>, вне зависимости от присваиваемой квалификации являются:</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><em>[данный пункт составляется в соответствии с п. 4.2 ФГОС ВО]</em></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>2.3.</strong> <strong>Виды профессиональной деятельности</strong>, к которым готовятся выпускники, освоившие программы <em>бакалавриата/специалитета/магистратуры</em> с присвоением квалификации &laquo;_______________________&raquo;:</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><em>[данный пункт составляется в соответствии с п. 4.3 ФГОС ВО]</em></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt">Основной(-ые) вид(-ы) деятельности:</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt">- </span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt">Дополнительный(-ые) вид(-ы) деятельности:</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt">- </span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><em>&nbsp;[При разработке и реализации программ бакалавриата/специалитета/магистратуры университет ориентируется на конкретный вид (виды) профессиональной деятельности, к которому (которым) готовится выпускник, исходя из потребностей рынка труда, научно-исследовательского и материально-технического ресурса университета и требований к результатам освоения образовательной программы]. </em></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>2.4. Задачи профессиональной деятельности выпускника</strong></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><em>[данный пункт составляется в соответствии с п. 4.4 ФГОС ВО]</em></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt">Бакалавр/специалист/магистр по направлению подготовки <strong>ХХ.ХХ.ХХ Название направления</strong> должен решать следующие профессиональные задачи в соответствии с видами профессиональной деятельности:</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong><em>Вид деятельности:</em></strong></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt">- задача;</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt">&hellip;</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><em>[Задачи профессиональной деятельности выпускника формулируются для каждого вида профессиональной деятельности по данному направлению и профилю подготовки ВО на основе соответствующих ФГОС ВО и ПрОПОП ВО и дополняются с учетом традиций университета и потребностей заинтересованных работодателей].</em></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>3. Требования к результатам освоения ОПОП бакалавриата/ специалитета/магистратуры </strong></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><span style="color:black"><em>[Данный пункт составляется в соответствии пп. 5.2-5.4 ФГОС ВО, с учетом пп. 2.3-2.4 данной ОПОП. Компетенции выпускника (ОК, ОПК, ПК), формируемые в процессе освоения данной ОПОП ВО, определяются на основе образовательного стандарта по соответствующему направлению подготовки/специальности/ магистерской программе].</em></span></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><span style="font-family:Arial,sans-serif">Выпускник, освоивший программу бакалавриата, должен обладать следующими <strong>общекультурными</strong> компетенциями (ОК):</span></span></strong></p>

<ul>
	<li style="text-align:justify"><strong><span style="font-size:12pt"><span style="color:black">компетенция (ОК);</span></span></strong></li>
	<li style="text-align:justify"><strong><span style="font-size:12pt"><span style="color:black">&hellip;</span></span></strong></li>
</ul>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><span style="font-family:Arial,sans-serif">Выпускник, освоивший программу бакалавриата, должен обладать следующими <strong>общепрофессиональными</strong> компетенциями (ОПК):</span></span></strong></p>

<ul>
	<li style="text-align:justify"><strong><span style="font-size:12pt"><span style="color:black">компетенция (ОПК);</span></span></strong></li>
	<li style="text-align:justify"><strong><span style="font-size:12pt"><span style="color:black">&hellip;</span></span></strong></li>
</ul>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><span style="font-family:Arial,sans-serif">Выпускник, освоивший программу бакалавриата, должен обладать <strong>профессиональными</strong> компетенциями (ПК), соответствующими виду (видам) профессиональной деятельности, на который (которые) ориентирована программа бакалавриата:</span></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong><em>Вид деятельности:</em></strong></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt">- компетенция (ПК);</span></strong></p>

<ul>
	<li style="text-align:justify"><strong><span style="font-size:12pt"><span style="color:black">&hellip;</span></span></strong></li>
</ul>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>4. Требования к структуре программы бакалавриата/специалитета/ магистратуры</strong></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><em>[Данный пункт составляется в соответствии с п. 6.2, 6.10, 6.11 ФГОС ВО]</em></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt">Структура программы бакалавриата/специалитета/магистратуры включает обязательную часть (базовую) и часть, формируемую участниками образовательных отношений (вариативную). Это обеспечивает возможность реализации программ бакалавриата/специалитета/магистратуры, имеющих различную направленность (профиль) образования в рамках одного направления подготовки (далее - направленность (профиль) программы).</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt">Программа бакалавриата/специалитета/магистратуры состоит из следующих блоков:</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt">Блок 1 &quot;Дисциплины (модули)&quot;, который включает дисциплины (модули), относящиеся к базовой части программы, и дисциплины (модули), относящиеся к ее вариативной части.</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt">Блок 2 &quot;Практики&quot; / &quot;Практики, в том числе научно-исследовательская работа (НИР)&quot;, который в полном объеме относится к вариативной части программы.</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt">Блок 3 &quot;Государственная итоговая аттестация&quot;, который в полном объеме относится к базовой части программы и завершается присвоением квалификации, указанной в перечне специальностей и направлений подготовки высшего образования, утверждаемом Министерством образования и науки Российской Федерации.</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt">При разработке программы бакалавриата/специалитета/магистратуры обучающимся обеспечивается возможность освоения дисциплин (модулей) по выбору, в том числе специальные условия инвалидам и лицам с ограниченными возможностями здоровья, в объеме не менее ХХ процентов вариативной части Блока 1 &quot;Дисциплины (модули)&quot;.</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt">Количество часов, отведенных на занятия лекционного типа в целом по Блоку 1 &quot;Дисциплины модули)&quot;, должно составлять не более ХХ процентов от общего количества часов аудиторных занятий, отведенных на реализацию данного Блока.</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt">Реализация компетентностного подхода предусматривает широкое использование в учебном процессе активных и интерактивных форм проведения занятий (компьютерных симуляций, деловых и ролевых игр, разбора конкретных ситуаций, психологических и иных тренингов) в сочетании с внеаудиторной работой с целью формирования и развития профессиональных навыков обучающихся. В рамках учебных курсов предусмотрены встречи с представителями российских и зарубежных компаний, государственных и общественных организаций, мастер-классы экспертов и специалистов. </span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>4.1. Календарный учебный график </strong>(Приложение 1)</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>4.2. Учебный план подготовки бакалавра/специалиста/магистра </strong>(Приложение 1)</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>4.3. Матрица компетенций </strong>(Приложение 2)</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>4.4. Рабочие программы учебных курсов, предметов, дисциплин (модулей)</strong> (Приложение 3)<em> </em></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:center"><strong><span style="font-size:12pt"><strong>Аннотации рабочих программ дисциплин</strong></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:center"><strong><span style="font-size:12pt"><strong>Базовая часть</strong></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm"><strong><span style="font-size:12pt"><strong>00.00.00 Название дисциплины</strong></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>Цель дисциплины:</strong> &hellip;. </span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>Задачи: </strong>&hellip;.</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>Требования к результатам освоения: </strong>в результате освоения дисциплины формируются следующие компетенции: &hellip; <em>[указываются только коды компетенций]</em>.</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>Место в учебном плане:</strong> Блок 1, базовая часть / вариативная часть / дисциплины по выбору, дисциплина осваивается в &hellip; семестрах. </span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>Общая трудоемкость: </strong>&hellip; з.е.</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>Основные образовательные технологии: </strong>&hellip; <em>[перечисляются технологии]</em>.</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>Краткое содержание: </strong>&hellip; .</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>Форма контроля:</strong> &hellip; семестры (зачет), &hellip; семестр (экзамен), &hellip; семестр (курсовая работа). </span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>Учебно-методическое и информационное обеспечение (основная литература): </strong></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt">1. &hellip; </span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt">2. &hellip;</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>Сведения о составителях: </strong>Фамилия И.О., степень, должность <em>[с указанием кафедры]</em>.<em> </em></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>4.5. Программы практик и организация научно-исследовательской работы обучающихся </strong>(Приложение 4)<em> </em></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify">&nbsp;</p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt">В Блок 2 &quot;Практики&quot; / &quot;Практики, в том числе научно-исследовательская работа (НИР)&quot; входят учебная и производственная, в том числе преддипломная, практики.</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt">Типы учебной практики: практика по получению первичных профессиональных умений и навыков.</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt">Типы производственной практики: практика по получению профессиональных умений и опыта профессиональной деятельности (в том числе технологическая практика, педагогическая практика); НИР.</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt">Способы проведения учебной и производственной практик: стационарная.</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt">Преддипломная практика проводится для выполнения выпускной квалификационной работы и является обязательной.</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt">Для лиц с ограниченными возможностями здоровья выбор мест прохождения практик должен учитывать состояние здоровья и требования по доступности.</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:center"><strong><span style="font-size:12pt"><strong>Аннотации программ практик</strong></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm"><strong><span style="font-size:12pt"><strong>4.5.1. Учебная практика </strong>(практика по получению &hellip;)</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>Цель: </strong>&hellip;. </span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>Задачи</strong>: &hellip;. </span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>Место в структуре ОПОП, общая трудоемкость: </strong>&hellip; семестр &hellip; недель (&hellip;з.е.).</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>Требования к результатам освоения: </strong>В результате проведения практики формируются следующие компетенции: ПК-1; ПК-2; ПК-3; ПК-4.</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>Основные образовательные технологии: </strong>&hellip; <em>[перечисляются технологии]</em>.</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>Краткое содержание: </strong>&hellip; .</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>Форма отчета: </strong>&hellip; <em>[дается краткое описание отчета и требований к нему]</em>.</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>Форма контроля: </strong>&hellip; семестр (дифференцированный зачет).</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>Учебно-методическое и информационное обеспечение (основная литература): </strong></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt">1. &hellip; </span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt">2. &hellip;</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>Сведения о составителях: </strong>Фамилия И.О., степень, должность <em>[с указанием кафедры]</em>.<em> </em></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm"><strong><span style="font-size:12pt"><strong>4.5.2. Производственная практика</strong></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><span style="background-color:white"><span style="color:black">&lt;&hellip;&gt;</span></span></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm"><strong><span style="font-size:12pt"><strong>4.5.3. Преддипломная практика</strong></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><span style="background-color:white"><span style="color:black">&lt;&hellip;&gt;</span></span></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>4.6. Государственная итоговая аттестация выпускников </strong>(Приложение 5)</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><span style="background-color:white">Государственная и<span style="color:black">тоговая аттестация выпускника университета является обязательной и осуществляется после освоения образовательной программы в полном объеме. В соответствии с требованиями ФГОС ВО по данному направлению подготовки государственная итоговая аттестация включает: &hellip;.</span></span></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><em>[В данном пункте указывается перечень кодов компетенций, проверка сформированности которых осуществляется в процессе подготовки и проведения ГИА, перечисляются основные требования к структуре и содержанию аттестационного испытания, критерии оценивания результатов обучения]</em></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>5. Требования к условиям реализации программы бакалавриата/специалитета/магистратуры</strong></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>5.1. Общесистемные требования к условиям реализации программы бакалавриата/специалитета/магистратуры</strong></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><span style="color:black">Ресурсное обеспечение ОПОП ВО формируется на основе требований к условиям реализации образовательных программ, определяемых ФГОС ВО, действующей нормативно-правовой базой, с учетом особенностей, связанных с уровнем и профилем образовательной программы. Ресурсное обеспечение ОПОП ВО определяется как в целом по ОПОП ВО, так и по циклам дисциплин и/или модулей.</span></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><em>[Данный пункт составляется в соответствии с п. 7.1 ФГОС ВО]</em></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>5.2. Кадровое обеспечение условий реализации программы бакалавриата/специалитета/магистратуры</strong></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><em>[Данный пункт составляется в соответствии с п. 7.2 ФГОС ВО]</em></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><em><span style="color:black">5.2.1. Реализация программы бакалавриата/специалитета/магистратуры обеспечивается руководящими и научно-педагогическими работниками организации, а также лицами, привлекаемыми к реализации программы бакалавриата/специалитета/магистратуры на условиях гражданско-правового договора.</span></em></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><em><span style="color:black">5.2.2. Доля научно-педагогических работников (в приведенных к целочисленным значениям ставок), имеющих образование, соответствующее профилю преподаваемой дисциплины (модуля), в общем числе научно-педагогических работников, реализующих программу бакалавриата/специалитета/магистратуры, должна составлять не менее ХХ процентов.</span></em></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><em><span style="color:black">5.2.3. Доля научно-педагогических работников (в приведенных к целочисленным значениям ставок), имеющих ученую степень (в том числе ученую степень, присвоенную за рубежом и признаваемую в Российской Федерации) и (или) ученое звание (в том числе ученое звание, полученное за рубежом и признаваемое в Российской Федерации), в общем числе научно-педагогических работников, реализующих программу бакалавриата/специалитета/магистратуры, должна быть не менее ХХ процентов.</span></em></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><em><span style="color:black">5.2.4. Доля работников (в приведенных к целочисленным значениям ставок) из числа</span></em></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><em><span style="color:black">руководителей и работников организаций, деятельность которых связана с направленностью (профилем) реализуемой программы бакалавриата/специалитета/магистратуры (имеющих стаж работы в данной профессиональной области не менее 3 лет) в общем числе работников, реализующих программу бакалавриата, должна быть не менее ХХ процентов.</span></em></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>5.3. Учебно-методическое и материально- техническое обеспечение условий реализации программы бакалавриата/специалитета/магистратуры</strong></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><em>[Данный пункт составляется в соответствии с п. 7.3 ФГОС ВО]</em></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>5.4.Финансовое обеспечение условий реализации программы бакалавриата/специалитета/магистратуры</strong></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><em>[Данный пункт составляется в соответствии с п. 7.4 ФГОС ВО]</em></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt">Финансовое обеспечение реализации программы бакалавриата/специалитета/магистратуры должно осуществляться в объеме не ниже установленных Министерством образования и науки Российской Федерации базовых нормативных затрат на оказание государственной услуги в сфере образования для данного уровня образования и направления подготовки с учетом корректирующих коэффициентов, учитывающих специфику образовательных программ в соответствии с Методикой определения нормативных затрат на оказание государственных услуг по реализации имеющих государственную аккредитацию образовательных программ высшего образования по специальностям и направлениям подготовки, утвержденной приказом Министерства образования и науки Российской Федерации от ________20__ г. N _____ (зарегистрирован Министерством юстиции Российской Федерации ____ 20__ г., регистрационный N ____).</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>6. Характеристики среды вуза, обеспечивающие развитие общекультурных (социально-личностных) компетенций выпускников</strong></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><em>.</em></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>7. Нормативно-методическое обеспечение системы оценки качества освоения обучающимися ОПОП бакалавриата/специалитета/магистратуры.</strong></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt">Университет обеспечивает гарантию качества подготовки, в том числе путем:</span></strong></p>

<ul>
	<li style="text-align:justify"><strong><span style="font-size:12pt"><span style="color:black">разработки стратегии по обеспечению качества подготовки выпускников с привлечением представителей работодателей;</span></span></strong></li>
	<li style="text-align:justify"><strong><span style="font-size:12pt"><span style="color:black">мониторинга, периодического рецензирования образовательных программ;</span></span></strong></li>
	<li style="text-align:justify"><strong><span style="font-size:12pt"><span style="color:black">разработки объективных процедур оценки уровня знаний и умений обучающихся, компетенций выпускников;</span></span></strong></li>
	<li style="text-align:justify"><strong><span style="font-size:12pt"><span style="color:black">обеспечения компетентности преподавательского состава;</span></span></strong></li>
	<li style="text-align:justify"><strong><span style="font-size:12pt"><span style="color:black">регулярного проведения самообследования по согласованным критериям для оценки деятельности (стратегии) и сопоставления с другими образовательными учреждениями с привлечением представителей работодателей;</span></span></strong></li>
	<li style="text-align:justify"><strong><span style="font-size:12pt"><span style="color:black">информирования общественности о результатах своей деятельности, планах, инновациях.</span></span></strong></li>
</ul>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt">Оценка качества освоения программ бакалавриата (специалитета, магистратуры) обучающимися включает текущий контроль успеваемости, промежуточную аттестацию обучающихся и государственную итоговую аттестацию.</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><em>[Обучающимся должна быть предоставлена возможность оценивания содержания, организации и качества образовательного процесса в целом и отдельных дисциплин (модулей) и практик, а также работы отдельных преподавателей. Далее указывается перечень нормативных документов, обеспечивающих текущий и итоговый контроль успеваемости и промежуточной аттестации обучающихся по ОПОП)]</em></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>7.1. Фонды оценочных средств для проведения текущего контроля успеваемости и промежуточной аттестации</strong></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt">Фонды оценочных средств и конкретные формы и процедуры текущего контроля знаний и промежуточной аттестации по каждой дисциплине содержатся в рабочих программах дисциплин и доводятся до сведения обучающихся в течение первых недель обучения.</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt">В целях приближения текущего контроля успеваемости и промежуточной аттестации обучающихся к задачам их будущей профессиональной деятельности, университет привлекает к процедурам текущего контроля успеваемости и промежуточной аттестации, а также экспертизе оценочных средств внешних экспертов &ndash; работодателей из числа действующих руководителей и работников организаций, деятельность которых связана с направленностью (профилем) реализуемой программы (имеющих стаж работы в данной профессиональной области не менее 3 лет), а также преподавателей смежных образовательных областей, специалистов по разработке и сертификации оценочных средств).</span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>8. Другие нормативно-методические документы и материалы, обеспечивающие качество подготовки обучающихся</strong></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm"><strong><span style="font-size:12pt"><em>(В данном разделе могут быть представлены документы и материалы, не нашедшие отражения в предыдущих разделах ОПОП, например:</em></span></strong></p>

<ul>
	<li style="text-align:justify"><strong><span style="font-size:12pt"><a name="_Toc149693844"></a><a name="_Toc149688277"></a><a name="_Toc149688221"><em>Описание механизмов функционирования при реализации данной ОПОП системы обеспечения качества подготовки,&nbsp; созданной в вузе, в том числе: мониторинга и&nbsp; периодического рецензирования образовательной программы; обеспечения компетентности преподавательского состава; регулярного проведения самообследования по согласованным критериям для оценки деятельности (стратегии); системы внешней оценки качества реализации ОПОП (учета и анализа мнений работодателей, выпускников вуза и других субъектов образовательного процесса)</em></a><em>;</em></span></strong></li>
	<li style="text-align:justify"><strong><span style="font-size:12pt"><a name="_Toc149693843"></a><a name="_Toc149688276"></a><a name="_Toc149688220"><em>Положение о балльно-рейтинговой системе оценивания</em></a><em>;</em></span></strong></li>
	<li style="text-align:justify"><strong><span style="font-size:12pt"><a name="_Toc149693845"></a><a name="_Toc149688278"></a><a name="_Toc149688222"><em>Соглашения (при их наличии) о порядке реализации совместных с зарубежными партнерами ОПОП и мобильности студентов и преподавателей</em></a><em> и т.д.)</em></span></strong></li>
</ul>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>9. Регламент по организации периодического обновления ОПОП ВО в целом и составляющих ее документов</strong></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><em>Образовательная программа ежегодно обновляется в какой-либо части (состав дисциплин, содержание рабочих программ дисциплин, программ практики, методические материалы и пр.) с учетом развития науки, техники, культуры, экономики, технологий, социально-культурной сферы.</em></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><em>Изменения в ОПОП осуществляются под руководством руководителя направления подготовки, согласуется с Ученым советом факультета, и оформляется в виде приложения к образовательной программе.</em></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong>&nbsp;</strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><strong>Приложения</strong></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><span style="font-size:11.0pt">Приложение 1.&nbsp;&nbsp;&nbsp; Учебный план и календарный учебный график</span></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><span style="font-size:11.0pt">Приложение 2.&nbsp;&nbsp;&nbsp; Матрица компетенций</span></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><span style="font-size:11.0pt">Приложение 3.&nbsp;&nbsp;&nbsp; Рабочие программ дисциплин (модулей)</span></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><span style="font-size:11.0pt">Приложение 4&nbsp;&nbsp;&nbsp;&nbsp; Программы практик</span></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><span style="font-size:11.0pt">Приложение 5.&nbsp;&nbsp;&nbsp; Программа государственной итоговой аттестации </span></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><span style="font-size:11.0pt">Приложение 6.&nbsp;&nbsp;&nbsp; Фонды оценочных средств для проведения текущего контроля успеваемости и промежуточной аттестации</span></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><span style="font-size:11.0pt">Приложение 7. &nbsp;&nbsp;&nbsp; Учебно-методические комплексы дисциплин</span></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><strong><span style="font-size:12pt"><span style="font-size:11.0pt">Приложение 7. &nbsp;&nbsp;&nbsp; Карта книгообеспеченности</span></span></strong></p>

<p><strong>&nbsp;</strong></p>

<p style="margin-left:0cm; margin-right:0cm; text-align:center"><strong><span style="font-size:12pt"><strong>Список разработчиков ОПОП, экспертов</strong></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm"><strong><span style="font-size:12pt"><strong>Разработчики: </strong></span></strong></p>

<table cellspacing="0" class="Table" style="border-collapse:collapse; border:undefined; width:475.9pt">
	<tbody>
		<tr>
			<td style="vertical-align:top; width:185.4pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:center"><strong><span style="font-size:12pt">Ученое звание, ученая степень, должность</span></strong></p>
			</td>
			<td style="width:171.0pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:center"><strong><span style="font-size:12pt">Подпись</span></strong></p>
			</td>
			<td style="width:119.5pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:center">&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top; width:185.4pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:center"><strong>&nbsp;</strong></p>
			</td>
			<td style="width:171.0pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:center"><strong>&nbsp;</strong></p>
			</td>
			<td style="width:119.5pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:center"><strong>&nbsp;</strong></p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top; width:185.4pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:center"><strong><span style="font-size:12pt">Ученое звание, ученая степень, должность</span></strong></p>
			</td>
			<td style="width:171.0pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:center"><strong><span style="font-size:12pt">Подпись</span></strong></p>
			</td>
			<td style="width:119.5pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:center">&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top; width:185.4pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:center"><strong>&nbsp;</strong></p>
			</td>
			<td style="width:171.0pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:center"><strong>&nbsp;</strong></p>
			</td>
			<td style="width:119.5pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:center"><strong>&nbsp;</strong></p>
			</td>
		</tr>
	</tbody>
</table>

<p style="margin-left:0cm; margin-right:0cm"><strong>&nbsp;</strong></p>

<p style="margin-left:0cm; margin-right:0cm"><strong><span style="font-size:12pt"><strong>Эксперты: </strong></span></strong></p>

<p style="margin-left:0cm; margin-right:0cm"><strong>&nbsp;</strong></p>

<table cellspacing="0" class="Table" style="border-collapse:collapse; border:undefined; width:475.9pt">
	<tbody>
		<tr>
			<td style="vertical-align:top; width:185.4pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:center"><strong><span style="font-size:12pt">Ученое звание, ученая степень, должность</span></strong></p>
			</td>
			<td style="width:171.0pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:center"><strong><span style="font-size:12pt">Подпись</span></strong></p>
			</td>
			<td style="width:119.5pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:center">&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top; width:185.4pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:center"><strong>&nbsp;</strong></p>
			</td>
			<td style="width:171.0pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:center"><strong>&nbsp;</strong></p>
			</td>
			<td style="width:119.5pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:center"><strong>&nbsp;</strong></p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top; width:185.4pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:center"><strong><span style="font-size:12pt">Ученое звание, ученая степень, должность</span></strong></p>
			</td>
			<td style="width:171.0pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:center"><strong><span style="font-size:12pt">Подпись</span></strong></p>
			</td>
			<td style="width:119.5pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:center">&nbsp;</p>
			</td>
		</tr>
		<tr>
			<td style="vertical-align:top; width:185.4pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:center"><strong>&nbsp;</strong></p>
			</td>
			<td style="width:171.0pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:center"><strong>&nbsp;</strong></p>
			</td>
			<td style="width:119.5pt">
			<p style="margin-left:0cm; margin-right:0cm; text-align:center"><strong>&nbsp;</strong></p>
			</td>
		</tr>
	</tbody>
</table>

