<ol class="breadcrumb">
	<li class="breadcrumb-item"><a href="/users/get"><?=$label?></a></li>
	<li class="breadcrumb-item active">Создание пользователя</li>
</ol>

<div class="alert alert-success hidden" style="display: none;" id="success-alert">
	Сохранено
</div>
<div class="alert alert-warning hidden" style="display: none;" id="warning-alert">
	Ошибка: 
</div>
<form role="form" action="/users/store" method="POST" ons ubmit="SendXHR(this); return false;">
	<div class="form-group">
		<label>Фамилия</label>
		<input required="required" autocomplete="off"  class="form-control" type="text" name="surname" placeholder="Фамилия">
	</div>
	<div class="form-group">
		<label>Имя</label>
		<input required="required" autocomplete="off"  class="form-control" type="text" name="name" placeholder="Имя">
	</div>
	<div class="form-group">
		<label>Отчество</label>
		<input required="required" autocomplete="off"  class="form-control" type="text" name="lastname" placeholder="Отчество">
	</div>
	<div class="form-group">
		<label>Логин</label>
		<input required="required" autocomplete="off"  class="form-control" type="text" name="login" placeholder="Логин" value="<?=$login?>">
	</div>

	<div class="form-group">
		<label>Пароль (временный)</label>
		<input required="required" autocomplete="off"  class="form-control" type="text" name="password" placeholder="Пароль" value="<?=$login?>">
	</div>

	<div class="form-group">
		<label>Роль в системе</label>
		<select multiple="multiple" class="form-control" name="role[]" id="role" placeholder="Роль в системеность">
			<?php foreach ($roles as $p) { ?>
				<option value="<?=$p[0]?>"><?=$p[1]?></option>
			<?php } ?>
		</select>
	</div>

	<div class="form-group">
		<label>E-mail</label>
		<input required="required" autocomplete="off"  class="form-control" type="text" name="email" placeholder="Электронная почта">
	</div>

	<div class="form-group">
		<label>Телефон</label>
		<input required="required" autocomplete="off"  class="form-control" type="text" name="phone" placeholder="Номер телефона">
	</div>

	<div class="form-group">
		<label>Факультет</label>
		<select class="form-control" name="faculty_id" id="faculty_id" placeholder="Факультет" onchange="loadForm(this, 'pulpit_id', 'getpulpits');">
			<option disabled selected>Выберите...</option>
			<?php foreach ($faculties as $p) { ?>
				<option value="<?=$p[0]?>"><?=$p[1]?></option>
			<?php } ?>
		</select>
	</div>

	<div class="form-group">
		<label>Кафедра</label>
		<select class="form-control" name="pulpit_id" id="pulpit_id" placeholder="Кафедра">

		</select>
	</div>

	<div class="form-group">
		<label>Научная степень</label>
		<select class="form-control" name="sc_level" id="sc_level" placeholder="Научная степень">
			<?php foreach ($sc_levels as $p) { ?>
				<option value="<?=$p[0]?>"><?=$p[1]?></option>
			<?php } ?>
		</select>
	</div>

	<div class="form-group">
		<label>Научное звание</label>
		<select class="form-control" name="sc_title" id="sc_title" placeholder="Научное звание">
			<?php foreach ($sc_titles as $p) { ?>
				<option value="<?=$p[0]?>"><?=$p[1]?></option>
			<?php } ?>
		</select>
	</div>

	<div class="checkbox">
		<label><input type="checkbox" name="opop_manager" onchange="opManager(this);" value="1"> Руководитель ОП</label>
	</div>


	<div class="form-group" id="opop_manager">
	</div>
	<div class="form-group" id="add_button_area">
		
	</div>

	<div class="form-group">
		<input required="required" autocomplete="off"  class="form-control btn btn-success" type="submit" value="Создать">
	</div>
</form>
