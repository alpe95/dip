<ol class="breadcrumb">
	<li class="breadcrumb-item"><a href="/users/get"><?=$label?></a></li>
	<li class="breadcrumb-item active">Редактирование пользователя</li>
</ol>

<div class="alert alert-success hidden" style="display: none;" id="success-alert">
	Сохранено
</div>
<div class="alert alert-warning hidden" style="display: none;" id="warning-alert">
	Ошибка: 
</div>
<form role="form" action="/users/update/<?=$_GET['id']?>" method="POST" ons ubmit="SendXHR(this); return false;">
	<div class="form-group">
		<label>Фамилия</label>
		<input required="required" autocomplete="off"  class="form-control" type="text" name="surname" placeholder="Фамилия" value="<?=$user->surname?>">
	</div>
	<div class="form-group">
		<label>Имя</label>
		<input required="required" autocomplete="off"  class="form-control" type="text" name="name" placeholder="Имя" value="<?=$user->name?>">
	</div>
	<div class="form-group">
		<label>Отчество</label>
		<input required="required" autocomplete="off"  class="form-control" type="text" name="lastname" placeholder="Отчество" value="<?=$user->lastname?>">
	</div>
	<div class="form-group">
		<label>Логин</label>
		<input required="required" autocomplete="off"  class="form-control" type="text" name="login" placeholder="Логин" value="<?=$user->login?>">
	</div>

	<div class="form-group">
		<label>Пароль (оставьте поле пустым, если изменение не требуется)</label>
		<input autocomplete="off"  class="form-control" type="text" name="password" placeholder="Пароль">
	</div>

	<div class="form-group">
		<label>Роль в системе</label>
		<select multiple="multiple" class="form-control" name="role[]" id="role" placeholder="Роль в системе">
			<?php 	

			foreach ($roleslist as $p) { 
				if (in_array($p[0], $user->roles)) {
					echo "<option selected value='$p[0]'>$p[1]</option>";
				}
				else {
					echo "<option value='$p[0]'>$p[1]</option>";
				}
			}

			?>
		</select>
	</div>

	<div class="form-group">
		<label>E-mail</label>
		<input required="required" autocomplete="off"  class="form-control" type="text" name="email" placeholder="Электронная почта" value="<?=$user->email?>">
	</div>

	<div class="form-group">
		<label>Телефон</label>
		<input required="required" autocomplete="off"  class="form-control" type="text" name="phone" placeholder="Номер телефона" value="<?=$user->phone?>">
	</div>

	<div class="form-group">
		<label>Факультет</label>
		<select class="form-control" name="faculty_id" id="faculty_id" placeholder="Факультет" onchange="loadForm(this, 'pulpit_id', 'getpulpits');" onload="loadForm(this, 'pulpit_id', 'getpulpits');">
			<option disabled selected>Выберите...</option>
			<?php foreach ($faculties as $p) { 

				if ($p[0] == $user->faculty_id) {
					echo "<option selected value='$p[0]'>$p[1]</option>";
				}
				else {
					echo "<option value='$p[0]'>$p[1]</option>";
				}
			} ?>
		</select>
	</div>

	<div class="form-group">
		<label>Кафедра</label>
		<select class="form-control" name="pulpit_id" id="pulpit_id" placeholder="Кафедра">
			<?php foreach ($pulpits as $p) { 

				if ($p[0] == $user->pulpit_id) {
					echo "<option selected value='$p[0]'>$p[1]</option>";
				}
				else {
					echo "<option value='$p[0]'>$p[1]</option>";
				}
			} ?>
		</select>
	</div>

	<div class="form-group">
		<label>Научная степень</label>
		<select class="form-control" name="sc_level" id="sc_level" placeholder="Научная степень">
			<?php foreach ($sc_levels as $p) { 

				if ($p[0] == $user->sc_level) {
					echo "<option selected value='$p[0]'>$p[1]</option>";
				}
				else {
					echo "<option value='$p[0]'>$p[1]</option>";
				}

			} ?>
		</select>
	</div>

	<div class="form-group">
		<label>Научное звание</label>
		<select class="form-control" name="sc_title" id="sc_title" placeholder="Научное звание">
			<?php foreach ($sc_titles as $p) { 

				if ($p[0] == $user->sc_title) {
					echo "<option selected value='$p[0]'>$p[1]</option>";
				}
				else {
					echo "<option value='$p[0]'>$p[1]</option>";
				}

			} ?>
		</select>
	</div>

	

	<div class="checkbox">
			<label><input type="checkbox" id="opop_checkbox" <?= $user->opop_manager==null?" ":"checked"?> name="opop_manager" onchange="opManager(this);" value="1"> Руководитель ОП</label>
	</div>


	<div class="form-group" id="opop_manager">
		<?php 
		$i = 1;
		foreach ($user->management as $m) { ?>

		<div class="opop_manager_item" id="omi_<?=$i?>">
			<label>Факультет</label> 
			<select class="form-control" name="opop_faculty_id[]" id="faculty_id_<?=$i?>" placeholder="Факультет" onchange="loadForm(this,'pulpit_id_<?=$i?>', 'getpulpits');">

				<?php 
					echo "<option disabled>Выберите...</option>";
					foreach ($faculties as $p) { 

					if ($m[2] == $p[0]) {
						echo "<option selected value='$p[0]'>$p[1]</option>";
					}
					else {
						echo "<option value='$p[0]'>$p[1]</option>";
					}

				} ?>
			</select> 

			<label>Кафедра</label> 
			<select class="form-control" name="opop_pulpit_id[]" id="pulpit_id_<?=$i?>" placeholder="Кафедра" onchange="loadForm(this, 'direction_id_<?=$i?>', 'getdirections');"> 
				<?php 
					$om_pulpits = mysqli_fetch_all(Database::Query("SELECT * FROM pulpits WHERE faculty_id='$m[2]'"));
					echo "<option disabled>Выберите...</option>";
					foreach ($om_pulpits as $p) { 

					if ($m[3] == $p[0]) {
						echo "<option selected value='$p[0]'>$p[1]</option>";
					}
					else {
						echo "<option value='$p[0]'>$p[1]</option>";
					}

				} ?>
			</select>     

			<label>Направление</label>     
			<select class="form-control" name="opop_direction_id[]" id="direction_id_<?=$i?>" placeholder="Направление" onchange="loadForm(this, 'profile_id_<?=$i?>', 'getprofiles');">  

			   <?php 
					$om_directions = mysqli_fetch_all(Database::Query("SELECT * FROM directions WHERE faculty_id='$m[2]'"));
					echo "<option disabled>Выберите...</option>";
					foreach ($om_directions as $p) { 

					if ($m[4] == $p[0]) {
						echo "<option selected value='$p[0]'>$p[1]</option>";
					}
					else {
						echo "<option value='$p[0]'>$p[1]</option>";
					}

				} ?>
			</select>     

		
			<select hidden class="form-control" name="opop_profile_id[]" id="profile_id_<?=$i?>" placeholder="Профиль"> 
				<?php 
					$om_profiles = mysqli_fetch_all(Database::Query("SELECT * FROM profiles WHERE direction_id='$m[4]'"));
					echo "<option value='0'>Выберите...</option>";
					foreach ($om_profiles as $p) { 

					if ($m[5] == $p[0] && $m[5]!=null) {
						echo "<option selected value='$p[0]'>$p[1]</option>";
					}
					else {
						echo "<option value='$p[0]'>$p[1]</option>";
					}

				} ?>
			</select> 

			<label>Форма обучения</label> 
			<select class="form-control" name="opop_study_type[]" id="study_type_<?=$i?>" placeholder="Форма обучения">
				<?php 
					$study_types = mysqli_fetch_all(Database::Query("SELECT * FROM study_types"));
					echo "<option disabled>Выберите...</option>";
					foreach ($study_types as $p) { 

					if ($m[6] == $p[0] && $m[6]!=null) {
						echo "<option selected value='$p[0]'>$p[1]</option>";
					}
					else {
						echo "<option value='$p[0]'>$p[1]</option>";
					}

				} ?>
			</select>
			<br> <!-- <button class="btn btn-danger" onclick="removeOMI(); return false;">Удалить</button> -->
		</div>

		<?php $i++; } ?>
	</div>
	<div class="form-group" id="add_button_area">
		
	</div>

	<div class="form-group">
		<input required="required" autocomplete="off"  class="form-control btn btn-success" type="submit" value="Сохранить">
	</div>
</form>
