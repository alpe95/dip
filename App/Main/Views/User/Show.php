<ol class="breadcrumb">
  <li class="breadcrumb-item active"><?=$label?></li>
</ol>
<div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Список пользователей <a href="/users/create" onclick="go(this); return false;" class="btn btn-primary" style="float: right; color: #fff;">Создать профиль</a></div>
        <div class="card-body">
          <div class="table-responsive">
            <?php if (count($users) > 0) { ?>
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Фамилия</th>
                  <th>Имя</th>
                  <th>Отчество</th>
                  <th>Факультет</th>
                  <th>Изменить</th>
                  <th>Удалить</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>Фамилия</th>
                  <th>Имя</th>
                  <th>Отчество</th>
                  <th>Факультет</th>
                  <th>Изменить</th>
                  <th>Удалить</th>
                </tr>
              </tfoot>
              <tbody>
                <?php foreach($users as $u) { ?>
                  <tr>
                    <td><?=$u[1]?></td>
                    <td><?=$u[2]?></td>
                    <td><?=$u[3]?></td>
                    <td><?=$u[4]?></td>
                    <td><a href="/users/edit/<?=$u[0]?>" onclick="go(this); return false;" class="btn btn-success" style="color: #fff;">Изменить</a></td>
                    <td><a href="/users/delete/<?=$u[0]?>" onclick="if(confirm('Удалить <?=$u[2]?> <?=$u[3]?>?')) { return true; }; return false;" class="btn btn-danger" style="color: #fff;">Удалить</a></td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
            <?php } ?>
          </div>
        </div>