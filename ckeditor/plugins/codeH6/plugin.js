CKEDITOR.plugins.add( 'codeH6', {
  icons: 'codeH6',
  init: function( editor ) {
    editor.addCommand( 'wrapH6', {
      exec: function( editor ) {
        editor.insertHtml('[t;]');
        $("#editor").setCursorPosition(-1);
      }
    });
    editor.ui.addButton( 'Короткое текстовое поле', {
      label: 'Короткое текстовое поле',
      command: 'wrapH6',
      toolbar: 'insert',
      icon: this.path + 'icons/t.png'
    });



    editor.addCommand( 'wrapH62', {
      exec: function( editor ) {
        editor.insertHtml('[n;]');
        $("#editor").setCursorPosition(-1);
      }
    });
    editor.ui.addButton( 'Поле для числа', {
      label: 'Поле для числа',
      command: 'wrapH62',
      toolbar: 'insert',
      icon: this.path + 'icons/n.png'
    });



    editor.addCommand( 'wrapH63', {
      exec: function( editor ) {
        editor.insertHtml('[l;]');
        $("#editor").setCursorPosition(-1);
      }
    });
    editor.ui.addButton( 'Поле для большого текста', {
      label: 'Поле для большого текста',
      command: 'wrapH63',
      toolbar: 'insert',
      icon: this.path + 'icons/l.png'
    });


    editor.addCommand( 'wrapH64', {
      exec: function( editor ) {
        editor.insertHtml('[opopmanager]');
        $("#editor").setCursorPosition(-1);
      }
    });
    editor.ui.addButton( 'Руководитель ОП', {
      label: 'Руководитель ОП',
      command: 'wrapH64',
      toolbar: 'insert',
      icon: this.path + 'icons/opopmanager.png'
    });

    editor.addCommand( 'wrapH65', {
      exec: function( editor ) {
        editor.insertHtml('[direction]');
        $("#editor").setCursorPosition(-1);
      }
    });
    editor.ui.addButton( 'Направление', {
      label: 'Направление',
      command: 'wrapH65',
      toolbar: 'insert',
      icon: this.path + 'icons/direction.png'
    });


    editor.addCommand( 'wrapH66', {
      exec: function( editor ) {
        editor.insertHtml('[profile]');
        $("#editor").setCursorPosition(-1);
      }
    });
    editor.ui.addButton( 'Профиль', {
      label: 'Профиль',
      command: 'wrapH66',
      toolbar: 'insert',
      icon: this.path + 'icons/profile.png'
    });

    editor.addCommand( 'wrapH67', {
      exec: function( editor ) {
        editor.insertHtml('[studytype]');
        $("#editor").setCursorPosition(-1);
      }
    });
    editor.ui.addButton( 'Форма обучения', {
      label: 'Форма обучения',
      command: 'wrapH67',
      toolbar: 'insert',
      icon: this.path + 'icons/study_type.png'
    });

    editor.addCommand( 'wrapH676', {
      exec: function( editor ) {
        editor.insertHtml('[studylevel]');
        $("#editor").setCursorPosition(-1);
      }
    });
    editor.ui.addButton( 'Уровень образования', {
      label: 'Уровень образования',
      command: 'wrapH676',
      toolbar: 'insert',
      icon: this.path + 'icons/study_level.png'
    });

    editor.addCommand( 'wrapH677', {
      exec: function( editor ) {
        editor.insertHtml('[faculty]');
        $("#editor").setCursorPosition(-1);
      }
    });
    editor.ui.addButton( 'Факультет', {
      label: 'Факультет',
      command: 'wrapH677',
      toolbar: 'insert',
      icon: this.path + 'icons/faculty.png'
    });

    editor.addCommand( 'wrapH678', {
      exec: function( editor ) {
        editor.insertHtml('[pulpit]');
        $("#editor").setCursorPosition(-1);
      }
    });
    editor.ui.addButton( 'Кафедра', {
      label: 'Кафедра',
      command: 'wrapH678',
      toolbar: 'insert',
      icon: this.path + 'icons/pulpit.png'
    });

    editor.addCommand( 'wrapH68', {
      exec: function( editor ) {
        editor.insertHtml('[facultymanager]');
        $("#editor").setCursorPosition(-1);
      }
    });
    editor.ui.addButton( 'Декан', {
      label: 'Декан факультета',
      command: 'wrapH68',
      toolbar: 'insert',
      icon: this.path + 'icons/facultymanager.png'
    });

  }
});