<?php
	session_start();
	require_once "App\Core\Routes.php";
	require_once "App\Main\System\Database.php";
	require_once 'App\Main\System\Models.php';
	require_once 'App\Main\System\Controllers.php';
	require_once 'App\Classes\Common.php';

	$IsAuthorised = false;

	if (isset($_SESSION['id']) && isset($_SESSION['hash'])) {

		$IsAuthorised = true;
		$Me = new Me();
	}

	return Routes::Start();

?>