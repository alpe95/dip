$(document).ready(function(){
		if ($("#opop_checkbox").attr("checked")) {
			$("#add_button_area").html("<div class='form_group'><button class='btn btn-success' onclick='addOp(); return false;'>+ Добавить</button></div>");
		}
	})
function bodyResize() {
	$(".dialogs-item-outer").height($(".messages").height() - 40);
}

function selector(button) {
	$(".navigation-buttons").removeClass("selected");
	$("#"+button.id).addClass("selected");

	$(".settings-content").hide();
	$("#"+button.id+"-block").show();
};

window.onpopstate = function(event) {
	$(".container-fluid").load(event.srcElement.location.href+"?&ajax=true");
}

function insertTextAtCursor(el, text, offset) {
    let val = el.value, endIndex, range, doc = el.ownerDocument;
    if (typeof el.selectionStart == "number"
            && typeof el.selectionEnd == "number") {
        endIndex = el.selectionEnd;
        el.value = val.slice(0, endIndex) + text + val.slice(endIndex);
        el.selectionStart = el.selectionEnd = endIndex + text.length+(offset?offset:0);
    } else if (doc.selection != "undefined" && doc.selection.createRange) {
        el.focus();
        range = doc.selection.createRange();
        range.collapse(false);
        range.text = text;
        range.select();
    }
}

	document.onkeydown = function(e) {

	  if (e.ctrlKey && e.keyCode == 'T'.charCodeAt(0)) {
	   	insertTextAtCursor($('#editor'), '[t;]');
	    return false;
	  }

	};

function go(link) {
	window.location.href = link.href;
	return;
	console.log(link.href);
	$(".container-fluid").html("<center><img width='100px' style='margin-top: 15%; opacity: 0.5;' src='Files/loading.gif'></center>");
	$(".container-fluid").load(link.href+"?&ajax=true");
	history.pushState(null, null, link.href);
}

function SendXHR(target) {
	let formData = new FormData(target);
	let action = target.action;
	let method = target.method;
	let xhr = new XMLHttpRequest();
	xhr.open(method,action,false);
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4) {
			if (xhr.responseText == "ok") {
				$('#success-alert').show();
				setTimeout("$('#success-alert').hide(1000);", 2500);
			}
			else{
				$('#warning-alert').show();
				$('#warning-alert').html("Ошибка: "+xhr.responseText);
				setTimeout("$('#warning-alert').hide(1000);", 4000);
			}
		}
	}
	xhr.send(formData);
}

function loadForm(target, type, api) {
	type = type.trim();
	let formData = null;
	let action = "/api/"+api;
	if (target != null)
		action += "?id="+target.value;
	let method = "GET";
	let xhr = new XMLHttpRequest();
	xhr.open(method,action,false);
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4) {
			console.log(xhr.responseText);
			let jsn = JSON.parse(xhr.responseText);
			let res = "<option selected value='0'>Выберите...</option>";
			for (let i = 0; i < jsn.length; i++) {
				res += "<option value='"+jsn[i].id+"'>"+jsn[i].title+"</option>";
			}
			$("#"+type).html(res);
		}
	}
	xhr.send(formData);
}

function loadFormNew(target, type, api) {
	type = type.trim();
	let formData = null;
	let action = "/api/"+api;
	var index = $("#direction_id")[0].selectedIndex;
	var dirID = $("#direction_id")[0].options[index].value;
	if (target != null)
		action += "?id="+target.value+"&id2="+dirID;
	let method = "GET";
	let xhr = new XMLHttpRequest();
	xhr.open(method,action,false);
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4) {
			console.log(xhr.responseText);
			let jsn = JSON.parse(xhr.responseText);
			let res = "<option selected value='0'>Выберите...</option>";
			for (let i = 0; i < jsn.length; i++) {
				res += "<option value='"+jsn[i].id+"'>"+jsn[i].title+"</option>";
			}
			$("#"+type).html(res);
		}
	}
	xhr.send(formData);
}

function opManager(target) {
	// console.log(target);
	if (target.checked == true) {
		$("#add_button_area").html("<div class='form_group'><button class='btn btn-success' onclick='addOp(); return false;'>+ Добавить</button></div>");
	} 
	else {
		$("#add_button_area").html("");
		$("#opop_manager").html("");
	}
}

function addOp() {
	let count = $(".opop_manager_item").length + 1;
	while ($("div").is("#omi_"+count)) {
		count++;
	}
	$("#opop_manager").append("<div class = 'opop_manager_item' id='omi_"+count+"'><label>Факультет</label> <select class='form-control' name='opop_faculty_id[]' id='faculty_id_"+count+"' placeholder='Факультет' onchange='loadForm(this,\"pulpit_id_"+count+"\", \"getpulpits\");loadForm(this, \"direction_id_"+count+"\", \"getdirections\");'> </select> <label>Кафедра</label> <select class='form-control' name='opop_pulpit_id[]' id='pulpit_id_"+count+"' placeholder='Кафедра' onchange=''> </select>     <label>Направление</label>     <select class='form-control' name='opop_direction_id[]' id='direction_id_"+count+"' placeholder='Направление' onchange='loadForm(this, \"profile_id_"+count+"\", \"getprofiles\");'>     </select>    <select class='form-control' name='opop_profile_id[]' id='profile_id_"+count+"' placeholder='Профиль' hidden> </select> <label>Форма обучения</label> <select class='form-control' name='opop_study_type[]' id='study_type_"+count+"' placeholder='Форма обучения'> </select><br> <button class='btn btn-danger' onclick='removeOMI("+count+"); return false;'>Удалить</button </div>");
	loadForm(null, "faculty_id_"+count, "getfaculties");
	loadForm(null, "study_type_"+count, "getstudytypes");
}

function removeOMI(id){
	$("#omi_"+id).remove();
}
